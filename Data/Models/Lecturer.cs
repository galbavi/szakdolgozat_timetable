﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Lecturer
    {
        public Lecturer()
        {
            LecturerType = new HashSet<LecturerType>();
            LecturerAvailable = new HashSet<LecturerAvailable>();
            SubjectLecturerConnect = new HashSet<SubjectLecturerConnect>();

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LecturerId { get; set; }

        [Required]
        public string Name { get; set; }

        [InverseProperty("Lecturer")]
        public virtual ICollection<LecturerType> LecturerType { get; set; }

        [InverseProperty("Lecturer")]
        public virtual ICollection<LecturerAvailable> LecturerAvailable { get; set; }

        [InverseProperty("Lecturer")]
        public virtual ICollection<SubjectLecturerConnect> SubjectLecturerConnect { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
