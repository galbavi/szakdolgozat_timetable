﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Subject
    {
        public Subject()
        {
            Course = new HashSet<Course>();
            GroupLine = new HashSet<GroupLine>();
            SubjectLecturerConnect = new HashSet<SubjectLecturerConnect>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubjectId { get; set; }

        [Required]
        public string Name { get; set; }

        [InverseProperty("Subject")]
        public virtual ICollection<Course> Course { get; set; }

        [InverseProperty("Subject")]
        public virtual ICollection<GroupLine> GroupLine { get; set; }

        [InverseProperty("Subject")]
        public virtual ICollection<SubjectLecturerConnect> SubjectLecturerConnect { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
