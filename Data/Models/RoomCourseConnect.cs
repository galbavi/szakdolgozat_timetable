﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class RoomCourseConnect
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoomCourseConnectId { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("Room")]
        public int RoomId { get; set; }

        [Key]
        [Column(Order = 3)]
        [ForeignKey("Course")]
        public int SubjectId { get; set; }

        [Key]
        [Column(Order = 4)]
        [ForeignKey("Course")]
        public int CourseId { get; set; }
        
        public virtual Course Course { get; set; }
        public virtual Room Room { get; set; }

        public override string ToString()
        {
            return Room.Name;
        }
    }
}
