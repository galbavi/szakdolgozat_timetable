﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class LecturerAvailable
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LecturerAvailableId { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("Lecturer")]
        public int LecturerId { get; set; }

        public WeekDay? DayAvailable { get; set; }

        [Required]
        public TimeSpan StartTimeAvailable { get; set; }

        [Required]
        public TimeSpan EndTimeAvailable { get; set; }

        public virtual Lecturer Lecturer { get; set; }

        public override string ToString()
        {
            return EnumExtension.GetDescription(DayAvailable) + " | " + StartTimeAvailable.ToString(@"hh\:mm") + " - " + EndTimeAvailable.ToString(@"hh\:mm");
        }
    }
}
