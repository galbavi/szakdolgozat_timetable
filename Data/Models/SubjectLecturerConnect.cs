﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class SubjectLecturerConnect
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubjectLecturerConnectId { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("Subject")]
        public int SubjectId { get; set; }

        [Key]
        [Column(Order = 3)]
        [ForeignKey("Lecturer")]
        public int LecturerId { get; set; }
        
        public virtual Lecturer Lecturer { get; set; }
        public virtual Subject Subject { get; set; }

        public override string ToString()
        {
            return Lecturer.Name;
        }
    }
}
