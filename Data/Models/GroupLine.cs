﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class GroupLine
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupLineId { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("GroupHeader")]
        public int GroupHeaderId { get; set; }

        [Key]
        [Column(Order = 3)]
        [ForeignKey("Subject")]
        public int SubjectId { get; set; }

        public virtual GroupHeader GroupHeader { get; set; }
        public virtual Subject Subject { get; set; }

        public override string ToString()
        {
            return Subject.Name;
        }
    }
}
