﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class TimetableHeader
    {
        private const string format = "{0} \nKészítésideje: {1} \nKurzusok száma: {2}";

        public TimetableHeader()
        {
            TimetableLine = new HashSet<TimetableLine>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TimetableHeaderId { get; set; }
        public string Name { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public DateTime ProductionDate { get; set; }

        [InverseProperty("TimetableHeader")]
        public virtual ICollection<TimetableLine> TimetableLine { get; set; }

        public override string ToString()
        {
            return String.Format(format, Name, ProductionDate, TimetableLine.Count);
        }
    }
}
