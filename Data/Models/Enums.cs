﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public enum WeekDay
    {
        [Description("Hétfő")]
        Monday,
        [Description("Kedd")]
        Tuesday,
        [Description("Szerda")]
        Wednesday,
        [Description("Csütörtök")]
        Thursday,
        [Description("Péntek")]
        Friday
    }

    public enum CourseType
    {
        [Description("Előadás")]
        Theoretical,
        [Description("Gyakorlat")]
        Practical,
        [Description("Labor")]
        Labour
    }

    public class EnumExtension
    {
        public static string GetDescription(Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return en.ToString();
        }
    }
}
