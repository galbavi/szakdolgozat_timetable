﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Room
    {
        public Room()
        {
            RoomCourseConnect = new HashSet<RoomCourseConnect>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoomId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int HeadCount { get; set; }

        [InverseProperty("Room")]
        public virtual ICollection<RoomType> RoomType { get; set; }

        [InverseProperty("Room")]
        public virtual ICollection<RoomCourseConnect> RoomCourseConnect { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
