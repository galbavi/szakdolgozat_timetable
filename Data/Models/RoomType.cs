﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class RoomType
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoomTypeId { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("Room")]
        public int RoomId { get; set; }

        [Key]
        [Column(Order = 3)]
        public CourseType Type { get; set; }

        public virtual Room Room { get; set; }

        public override string ToString()
        {
            return EnumExtension.GetDescription(Type);
        }
    }
}
