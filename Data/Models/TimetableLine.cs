﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class TimetableLine
    {

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TimetableLineId { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("TimetableHeader")]
        public int TimetableHeaderId { get; set; }

        public string SubjectName { get; set; }

        public string CourseName { get; set; }

        public string LecturerName { get; set; }

        public string RoomName { get; set; }

        public CourseType Type { get; set; }

        public int NumberOfClasses { get; set; }

        public int HeadCount { get; set; }

        public WeekDay Day { get; set; }

        public TimeSpan StartTime { get; set; }

        public virtual TimetableHeader TimetableHeader { get; set; }

        public override string ToString()
        {
            return String.Format("{0}\n{1} - {2}\n{3}\n{4}", CourseName, StartTime, (StartTime + TimeSpan.FromMinutes(45 * NumberOfClasses)), LecturerName, RoomName);
        }
    }
}
