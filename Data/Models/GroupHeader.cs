﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class GroupHeader
    {
        public GroupHeader()
        {
            GroupLine = new HashSet<GroupLine>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupHeaderId { get; set; }

        [Required]
        public string Name { get; set; }

        [InverseProperty("GroupHeader")]
        public virtual ICollection<GroupLine> GroupLine { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
