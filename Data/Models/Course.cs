﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Course
    {
        public Course()
        {
            RoomCourseConnect = new HashSet<RoomCourseConnect>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CourseId { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("Subject")]
        public int SubjectId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public CourseType Type { get; set; }

        [Required]
        public int NumberOfClasses { get; set; }

        [Required]
        public int HeadCount { get; set; }

        public WeekDay? Day { get; set; }

        public TimeSpan? StartTime { get; set; }

        public virtual Subject Subject { get; set; }

        [InverseProperty("Course")]
        public virtual ICollection<RoomCourseConnect> RoomCourseConnect { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
