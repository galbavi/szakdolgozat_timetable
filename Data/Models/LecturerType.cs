﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class LecturerType
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LecturerTypeId { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("Lecturer")]
        public int LecturerId { get; set; }

        [Key]
        [Column(Order = 3)]
        public CourseType Type { get; set; }

        public virtual Lecturer Lecturer { get; set; }

        public override string ToString()
        {
            return EnumExtension.GetDescription(Type);
        }
    }
}
