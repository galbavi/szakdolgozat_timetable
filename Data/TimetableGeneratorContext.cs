﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;

namespace Data
{
    public class TimetableGeneratorContext : DbContext
    {
        public TimetableGeneratorContext() : base("name=TimetableGenDatabase")
        {
        }

        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<GroupHeader> GroupHeader { get; set; }
        public virtual DbSet<GroupLine> GroupLine { get; set; }
        public virtual DbSet<Lecturer> Lecturer { get; set; }
        public virtual DbSet<LecturerAvailable> LecturerAvailable { get; set; }
        public virtual DbSet<LecturerType> LecturerType { get; set; }
        public virtual DbSet<Room> Room { get; set; }
        public virtual DbSet<RoomType> RoomType { get; set; }
        public virtual DbSet<RoomCourseConnect> RoomCourseConnect { get; set; }
        public virtual DbSet<Subject> Subject { get; set; }
        public virtual DbSet<SubjectLecturerConnect> SubjectLecturerConnect { get; set; }
        public virtual DbSet<TimetableHeader> TimetableHeader { get; set; }
        public virtual DbSet<TimetableLine> TimetableLine { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
