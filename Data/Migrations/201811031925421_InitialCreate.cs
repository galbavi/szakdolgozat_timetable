namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseId = c.Int(nullable: false, identity: true),
                        SubjectId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Type = c.Int(nullable: false),
                        NumberOfClasses = c.Int(nullable: false),
                        HeadCount = c.Int(nullable: false),
                        Day = c.Int(),
                        StartTime = c.Time(precision: 7),
                    })
                .PrimaryKey(t => new { t.CourseId, t.SubjectId })
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.RoomCourseConnects",
                c => new
                    {
                        RoomCourseConnectId = c.Int(nullable: false, identity: true),
                        RoomId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                        CourseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoomCourseConnectId, t.RoomId, t.SubjectId, t.CourseId })
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => new { t.SubjectId, t.CourseId }, cascadeDelete: true)
                .Index(t => t.RoomId)
                .Index(t => new { t.SubjectId, t.CourseId });
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        RoomId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        HeadCount = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RoomId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.SubjectId);
            
            CreateTable(
                "dbo.GroupLines",
                c => new
                    {
                        GroupLineId = c.Int(nullable: false, identity: true),
                        GroupHeaderId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupLineId, t.GroupHeaderId, t.SubjectId })
                .ForeignKey("dbo.GroupHeaders", t => t.GroupHeaderId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.GroupHeaderId)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.GroupHeaders",
                c => new
                    {
                        GroupHeaderId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.GroupHeaderId);
            
            CreateTable(
                "dbo.SubjectLecturerConnects",
                c => new
                    {
                        SubjectLecturerConnectId = c.Int(nullable: false, identity: true),
                        SubjectId = c.Int(nullable: false),
                        LecturerId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SubjectLecturerConnectId, t.SubjectId, t.LecturerId, t.Type })
                .ForeignKey("dbo.Lecturers", t => t.LecturerId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.SubjectId)
                .Index(t => t.LecturerId);
            
            CreateTable(
                "dbo.Lecturers",
                c => new
                    {
                        LecturerId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.LecturerId);
            
            CreateTable(
                "dbo.LecturerAvailables",
                c => new
                    {
                        LecturerAvailableId = c.Int(nullable: false, identity: true),
                        LecturerId = c.Int(nullable: false),
                        DayAvailable = c.Int(),
                        StartTimeAvailable = c.Time(nullable: false, precision: 7),
                        EndTimeAvailable = c.Time(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => new { t.LecturerAvailableId, t.LecturerId })
                .ForeignKey("dbo.Lecturers", t => t.LecturerId, cascadeDelete: true)
                .Index(t => t.LecturerId);
            
            CreateTable(
                "dbo.LecturerTypes",
                c => new
                    {
                        LecturerTypeId = c.Int(nullable: false, identity: true),
                        LecturerId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LecturerTypeId, t.LecturerId, t.Type })
                .ForeignKey("dbo.Lecturers", t => t.LecturerId, cascadeDelete: true)
                .Index(t => t.LecturerId);
            
            CreateTable(
                "dbo.TimetableHeaders",
                c => new
                    {
                        TimetableHeaderId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProductionDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TimetableHeaderId);
            
            CreateTable(
                "dbo.TimetableLines",
                c => new
                    {
                        TimetableLineId = c.Int(nullable: false, identity: true),
                        TimetableHeaderId = c.Int(nullable: false),
                        SubjectName = c.String(),
                        CourseName = c.String(),
                        LecturerName = c.String(),
                        RoomName = c.String(),
                        Type = c.Int(nullable: false),
                        NumberOfClasses = c.Int(nullable: false),
                        HeadCount = c.Int(nullable: false),
                        Day = c.Int(nullable: false),
                        StartTime = c.Time(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => new { t.TimetableLineId, t.TimetableHeaderId })
                .ForeignKey("dbo.TimetableHeaders", t => t.TimetableHeaderId, cascadeDelete: true)
                .Index(t => t.TimetableHeaderId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TimetableLines", "TimetableHeaderId", "dbo.TimetableHeaders");
            DropForeignKey("dbo.SubjectLecturerConnects", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.SubjectLecturerConnects", "LecturerId", "dbo.Lecturers");
            DropForeignKey("dbo.LecturerTypes", "LecturerId", "dbo.Lecturers");
            DropForeignKey("dbo.LecturerAvailables", "LecturerId", "dbo.Lecturers");
            DropForeignKey("dbo.GroupLines", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.GroupLines", "GroupHeaderId", "dbo.GroupHeaders");
            DropForeignKey("dbo.Courses", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.RoomCourseConnects", new[] { "SubjectId", "CourseId" }, "dbo.Courses");
            DropForeignKey("dbo.RoomCourseConnects", "RoomId", "dbo.Rooms");
            DropIndex("dbo.TimetableLines", new[] { "TimetableHeaderId" });
            DropIndex("dbo.LecturerTypes", new[] { "LecturerId" });
            DropIndex("dbo.LecturerAvailables", new[] { "LecturerId" });
            DropIndex("dbo.SubjectLecturerConnects", new[] { "LecturerId" });
            DropIndex("dbo.SubjectLecturerConnects", new[] { "SubjectId" });
            DropIndex("dbo.GroupLines", new[] { "SubjectId" });
            DropIndex("dbo.GroupLines", new[] { "GroupHeaderId" });
            DropIndex("dbo.RoomCourseConnects", new[] { "SubjectId", "CourseId" });
            DropIndex("dbo.RoomCourseConnects", new[] { "RoomId" });
            DropIndex("dbo.Courses", new[] { "SubjectId" });
            DropTable("dbo.TimetableLines");
            DropTable("dbo.TimetableHeaders");
            DropTable("dbo.LecturerTypes");
            DropTable("dbo.LecturerAvailables");
            DropTable("dbo.Lecturers");
            DropTable("dbo.SubjectLecturerConnects");
            DropTable("dbo.GroupHeaders");
            DropTable("dbo.GroupLines");
            DropTable("dbo.Subjects");
            DropTable("dbo.Rooms");
            DropTable("dbo.RoomCourseConnects");
            DropTable("dbo.Courses");
        }
    }
}
