namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePluralizing : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Courses", newName: "Course");
            RenameTable(name: "dbo.RoomCourseConnects", newName: "RoomCourseConnect");
            RenameTable(name: "dbo.Rooms", newName: "Room");
            RenameTable(name: "dbo.Subjects", newName: "Subject");
            RenameTable(name: "dbo.GroupLines", newName: "GroupLine");
            RenameTable(name: "dbo.GroupHeaders", newName: "GroupHeader");
            RenameTable(name: "dbo.SubjectLecturerConnects", newName: "SubjectLecturerConnect");
            RenameTable(name: "dbo.Lecturers", newName: "Lecturer");
            RenameTable(name: "dbo.LecturerAvailables", newName: "LecturerAvailable");
            RenameTable(name: "dbo.LecturerTypes", newName: "LecturerType");
            RenameTable(name: "dbo.TimetableHeaders", newName: "TimetableHeader");
            RenameTable(name: "dbo.TimetableLines", newName: "TimetableLine");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.TimetableLine", newName: "TimetableLines");
            RenameTable(name: "dbo.TimetableHeader", newName: "TimetableHeaders");
            RenameTable(name: "dbo.LecturerType", newName: "LecturerTypes");
            RenameTable(name: "dbo.LecturerAvailable", newName: "LecturerAvailables");
            RenameTable(name: "dbo.Lecturer", newName: "Lecturers");
            RenameTable(name: "dbo.SubjectLecturerConnect", newName: "SubjectLecturerConnects");
            RenameTable(name: "dbo.GroupHeader", newName: "GroupHeaders");
            RenameTable(name: "dbo.GroupLine", newName: "GroupLines");
            RenameTable(name: "dbo.Subject", newName: "Subjects");
            RenameTable(name: "dbo.Room", newName: "Rooms");
            RenameTable(name: "dbo.RoomCourseConnect", newName: "RoomCourseConnects");
            RenameTable(name: "dbo.Course", newName: "Courses");
        }
    }
}
