﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Insert(TEntity newentity);
        void InsertMore(IEnumerable<TEntity> entitiesToInsert);
        void Delete(TEntity entityToDelete);
        void DeleteMore(IEnumerable<TEntity> entitiesToDelete);
        IQueryable<TEntity> All { get; }
    }
}