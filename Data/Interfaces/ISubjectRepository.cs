﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;

namespace Data.Interfaces
{
    public interface ISubjectRepository : IRepository<Subject>
    {
        void Modify(int id, Subject modifiedSubject);
    }
}
