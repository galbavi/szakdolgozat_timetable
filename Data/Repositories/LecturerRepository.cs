﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Models;

namespace Data.Repositories
{
    public class LecturerRepository : TimetableGenRepo<Lecturer>, ILecturerRepository
    {
        public LecturerRepository(DbContext entities) : base(entities)
        {

        }

        public void Modify(int id, Lecturer modifiedLecturer)
        {
            var entity = entities.Set<Lecturer>().Single(x => x.LecturerId == modifiedLecturer.LecturerId);
            entities.Entry(entity).CurrentValues.SetValues(modifiedLecturer);
            if (entity.LecturerType.Count != modifiedLecturer.LecturerType.Count || modifiedLecturer.LecturerType.Count(x => x.LecturerTypeId == 0) > 0)
            {
                entity.LecturerType = modifiedLecturer.LecturerType;
            }
            if (entity.LecturerAvailable.Count != modifiedLecturer.LecturerAvailable.Count || modifiedLecturer.LecturerAvailable.Count(x => x.LecturerAvailableId == 0) > 0)
            {
                entity.LecturerAvailable = modifiedLecturer.LecturerAvailable;
            }
            entities.SaveChanges();
        }
    }
}
