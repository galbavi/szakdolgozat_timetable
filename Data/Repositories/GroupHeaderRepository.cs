﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Models;

namespace Data.Repositories
{
    public class GroupHeaderRepository : TimetableGenRepo<GroupHeader>, IGroupHeaderRepository
    {
        public GroupHeaderRepository(DbContext entities) : base(entities)
        {
        }

        public void Modify(int groupHeaderId, GroupHeader modifyGroupHeader)
        {
            var entity = entities.Set<GroupHeader>().Single(x => x.GroupHeaderId == modifyGroupHeader.GroupHeaderId);
            entities.Entry(entity).CurrentValues.SetValues(modifyGroupHeader);
            if (entity.GroupLine.Count != modifyGroupHeader.GroupLine.Count || modifyGroupHeader.GroupLine.Count(x => x.GroupHeaderId == 0) > 0)
            {
                entity.GroupLine = modifyGroupHeader.GroupLine;
            }

            entities.SaveChanges();
        }
    }
}
