﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Data.Models;
using Data.Interfaces;

namespace Data.Repositories
{
    public class CourseRepository : TimetableGenRepo<Course>, ICourseRepository
    {
        public CourseRepository(DbContext entities) : base(entities)
        {
        }

        public void Modify(int courseId, Course modifiedCourse)
        {
            var entity = entities.Set<Course>().Single(x => x.CourseId == modifiedCourse.CourseId);
            entities.Entry(entity).CurrentValues.SetValues(modifiedCourse);
            if (entity.RoomCourseConnect.Count != modifiedCourse.RoomCourseConnect.Count || modifiedCourse.RoomCourseConnect.Count(x => x.RoomCourseConnectId == 0) > 0)
            {
                entity.RoomCourseConnect = modifiedCourse.RoomCourseConnect;
            }
            entities.SaveChanges();
        }
    }
}
