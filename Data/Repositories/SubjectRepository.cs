﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Models;

namespace Data.Repositories
{
    public class SubjectRepository : TimetableGenRepo<Subject>, ISubjectRepository
    {
        public SubjectRepository(DbContext entities) : base(entities)
        {

        }
        public void Modify(int id, Subject modifiedSubject)
        {
            var entity = entities.Set<Subject>().Single(x => x.SubjectId == modifiedSubject.SubjectId);
            entities.Entry(entity).CurrentValues.SetValues(modifiedSubject);
            if (entity.SubjectLecturerConnect.Count != modifiedSubject.SubjectLecturerConnect.Count || modifiedSubject.SubjectLecturerConnect.Count(x => x.SubjectLecturerConnectId == 0) > 0)
            {
                entity.SubjectLecturerConnect = modifiedSubject.SubjectLecturerConnect;
            }
            entities.SaveChanges();
        }
    }
}
