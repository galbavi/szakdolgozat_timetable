﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Models;

namespace Data.Repositories
{
    public class RoomRepository : TimetableGenRepo<Room>, IRoomRepository
    {
        public RoomRepository(DbContext entities) : base(entities)
        {
        }

        public void Modify(int id, Room modifiedRoom)
        {
            var entity = entities.Set<Room>().Single(x => x.RoomId == modifiedRoom.RoomId);
            entities.Entry(entity).CurrentValues.SetValues(modifiedRoom);
            if (entity.RoomType.Count != modifiedRoom.RoomType.Count || modifiedRoom.RoomType.Count(x => x.RoomTypeId == 0) > 0)
            {
                entity.RoomType = modifiedRoom.RoomType;
            }

            entities.SaveChanges();
        }
    }
}
