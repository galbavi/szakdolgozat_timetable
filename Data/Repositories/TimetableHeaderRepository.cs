﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Models;

namespace Data.Repositories
{
    public class TimetableHeaderRepository : TimetableGenRepo<TimetableHeader>, ITimetableHeaderRepository
    {
        public TimetableHeaderRepository(DbContext entities) : base(entities)
        {
        }
    }
}
