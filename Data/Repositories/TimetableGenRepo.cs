﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Interfaces;

namespace Data.Repositories
{
    public abstract class TimetableGenRepo<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected DbContext entities;

        public TimetableGenRepo(DbContext entities)
        {
            this.entities = entities;
        }

        public void Delete(TEntity entityToDelete)
        {
            entities.Set<TEntity>().Remove(entityToDelete);
            entities.SaveChanges();
        }

        public void DeleteMore(IEnumerable<TEntity> entitiesToDelete)
        {
            entities.Set<TEntity>().RemoveRange(entitiesToDelete);
            entities.SaveChanges();
        }

        public IQueryable<TEntity> All => entities.Set<TEntity>();

        public void Insert(TEntity newentity)
        {
            entities.Set<TEntity>().Add(newentity);
            entities.SaveChanges();
        }

        public void InsertMore(IEnumerable<TEntity> entitiesToInsert)
        {
            entities.Set<TEntity>().AddRange(entitiesToInsert);
            entities.SaveChanges();
        }
    }
}
