﻿using BusinessLogic.DataMangement;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimetableGenerator.ViewModels;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for SubjectWindow.xaml
    /// </summary>
    public partial class SubjectWindow : Window
    {
        private SubjectViewModel subjectViewModel;
        private SubjectManagement subjectManagement;
        private CourseManagement courseManagement;

        public SubjectWindow(Data.TimetableGeneratorContext entities)
        {
            subjectManagement = new SubjectManagement(entities);
            courseManagement = new CourseManagement(entities);
            subjectViewModel = new SubjectViewModel();
            subjectViewModel.Subjects = subjectManagement.GetAllSubject();
            DataContext = subjectViewModel;
            InitializeComponent();
        }

        private void Click_AddSubject(object sender, RoutedEventArgs e)
        {
            new SubjectEditorWindow(subjectViewModel, subjectManagement, false).ShowDialog();
        }

        private void Click_DeleteSubject(object sender, RoutedEventArgs e)
        {
            if(subjectViewModel.SelectedSubject != null)
            {
                subjectManagement.RemoveSubject(subjectViewModel.SelectedSubject);
                subjectViewModel.Subjects.Remove(subjectViewModel.SelectedSubject);
                subjectViewModel.Courses = null;
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva tantárgy.", "Tantárgy törlés.", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DoubleClick_ModifySubject(object sender, MouseButtonEventArgs e)
        {
            if (subjectViewModel.SelectedSubject != null)
            {
                new SubjectEditorWindow(subjectViewModel, subjectManagement, true).ShowDialog();
            }
        }

        private void Click_AddCourse(object sender, RoutedEventArgs e)
        {
            if (subjectViewModel.SelectedSubject != null)
            {
                new CourseEditorWindow(subjectViewModel, courseManagement, false).ShowDialog();
            }
        }

        private void Click_DeleteCourse(object sender, RoutedEventArgs e)
        {
            if(subjectViewModel.SelectedCourse != null)
            {
                courseManagement.RemoveCourse(subjectViewModel.SelectedCourse);
                subjectViewModel.SelectedSubject.Course.Remove(subjectViewModel.SelectedCourse);
                subjectViewModel.Courses.Remove(subjectViewModel.SelectedCourse);
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva kurzus.", "Kurzus törlés.", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DoubleClick_ModifyCourse(object sender, MouseButtonEventArgs e)
        {
            if (subjectViewModel.SelectedCourse != null)
            {
                new CourseEditorWindow(subjectViewModel, courseManagement, true).ShowDialog();
            }
        }

        private void SelectionChanged_Subject(object sender, SelectionChangedEventArgs e)
        {
            if (subjectViewModel.Courses != null)
                subjectViewModel.Courses.Clear();
            if (subjectViewModel.SelectedSubject != null && subjectViewModel.SelectedSubject.Course != null)
            {
                foreach (var item in subjectViewModel.SelectedSubject.Course)
                {
                    subjectViewModel.Courses.Add(item);
                }
            }
        }
    }
}
