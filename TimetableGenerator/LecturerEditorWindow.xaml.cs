﻿using BusinessLogic.DataMangement;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimetableGenerator.ViewModels;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for LecturerEditorWindow.xaml
    /// </summary>
    public partial class LecturerEditorWindow : Window
    {
        private LecturerViewModel lecturerViewModel;
        private LecturerManagement lecturerManagement;
        private bool modify;

        public LecturerEditorWindow(LecturerViewModel lecturerViewModel, LecturerManagement lecturerManagement, bool modify)
        {
            this.lecturerViewModel = lecturerViewModel;
            this.lecturerManagement = lecturerManagement;
            this.modify = modify;
            DataContext = this.lecturerViewModel;
            this.lecturerViewModel.EditorLecturer = new Lecturer();
            if (this.modify)
            {
                this.lecturerViewModel.EditorLecturer.Name = this.lecturerViewModel.SelectedLecturer.Name;
                this.lecturerViewModel.EditorLecturer.LecturerId = this.lecturerViewModel.SelectedLecturer.LecturerId;
                this.lecturerViewModel.EditorLecturerTypes = 
                    new ObservableCollection<LecturerType>(this.lecturerViewModel.SelectedLecturer.LecturerType);
                this.lecturerViewModel.EditorLecturerAvailables =
                    new ObservableCollection<LecturerAvailable>(this.lecturerViewModel.SelectedLecturer.LecturerAvailable);
            }
            else
            {
                this.lecturerViewModel.EditorLecturerTypes = new ObservableCollection<LecturerType>();
                this.lecturerViewModel.EditorLecturerAvailables = new ObservableCollection<LecturerAvailable>();
            }
            InitializeComponent();
        }

        private void Click_AddCourseType(object sender, RoutedEventArgs e)
        {
            IEnumerator<LecturerType> enumerator = lecturerViewModel.EditorLecturerTypes.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Type == lecturerViewModel.SelectedCourseType)
                    return;
            }

            LecturerType newLecturerType = new LecturerType() { Type = lecturerViewModel.SelectedCourseType, };
            lecturerViewModel.EditorLecturerTypes.Add(newLecturerType);
        }

        private void Click_CloseLecturer(object sender, RoutedEventArgs e)
        {
            lecturerViewModel.EditorLecturer = null;
            this.Close();
        }

        private void Click_SaveLecturer(object sender, RoutedEventArgs e)
        {
            if (!lecturerManagement.CheckLecutrerTypes(lecturerViewModel.EditorLecturerTypes))
            {
                MessageBox.Show("Legalább egy típust kötelező megadni.", "Típus hiba!", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }

            lecturerViewModel.EditorLecturer.LecturerType = lecturerViewModel.EditorLecturerTypes;
            lecturerViewModel.EditorLecturer.LecturerAvailable = lecturerViewModel.EditorLecturerAvailables;

            if (!modify)
            {
                lecturerViewModel.Lecturers.Add(lecturerViewModel.EditorLecturer);
                lecturerManagement.AddNewLecturer(lecturerViewModel.EditorLecturer);
            }
            else
            {
                lecturerManagement.ModifyLecturer(lecturerViewModel.SelectedLecturer.LecturerId, lecturerViewModel.EditorLecturer);
                int index = lecturerViewModel.Lecturers.IndexOf(lecturerViewModel.SelectedLecturer);
                lecturerViewModel.Lecturers[index] = lecturerViewModel.EditorLecturer;
            }
            lecturerViewModel.SelectedLecturer = lecturerViewModel.EditorLecturer;
            this.Close();
        }

        private void Click_DeleteCourseType(object sender, RoutedEventArgs e)
        {
            lecturerViewModel.EditorLecturerTypes.Remove(lecturerViewModel.SelectedLecturerType);
        }

        private void Click_DeleteAllCourseType(object sender, RoutedEventArgs e)
        {
            lecturerViewModel.EditorLecturerTypes.Clear();
        }

        private void Click_DeleteAvaliability(object sender, RoutedEventArgs e)
        {
            lecturerViewModel.EditorLecturerAvailables.Remove(lecturerViewModel.SelectedLecturerAvailable);
        }

        private void Click_DeleteAllAvaliability(object sender, RoutedEventArgs e)
        {
            lecturerViewModel.EditorLecturerAvailables.Clear();
        }

        private void Click_AddAvaliability(object sender, RoutedEventArgs e)
        {
            LecturerAvailable newLecturerAvailable = new LecturerAvailable() { DayAvailable = lecturerViewModel.SelectedWeekDay };
            DateTime dt;
            if (!DateTime.TryParseExact(lecturerViewModel.EditorStartTime, "HH:mm", CultureInfo.InvariantCulture,
                                                          DateTimeStyles.None, out dt))
            {
                MessageBox.Show("A Kezdő időpontnak 00:00 és 23:59 között kell lennie hh:mm formátumban!", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            newLecturerAvailable.StartTimeAvailable = dt.TimeOfDay;

            if (!DateTime.TryParseExact(lecturerViewModel.EditorEndTime, "HH:mm", CultureInfo.InvariantCulture,
                                                          DateTimeStyles.None, out dt))
            {
                MessageBox.Show("A Vég időpontnak 00:00 és 23:59 között kell lennie hh:mm formátumban!", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            newLecturerAvailable.EndTimeAvailable = dt.TimeOfDay;

            if(newLecturerAvailable.EndTimeAvailable <= newLecturerAvailable.StartTimeAvailable)
            {
                MessageBox.Show("A Vég időpontnak nagyobbnak kell lennie mint a Kezdő időpont!", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (lecturerViewModel.EditorLecturerAvailables.Where(x => x.DayAvailable == newLecturerAvailable.DayAvailable
                    && x.StartTimeAvailable == newLecturerAvailable.StartTimeAvailable
                    && x.EndTimeAvailable == newLecturerAvailable.EndTimeAvailable).Count() > 0)
                return;

            lecturerViewModel.EditorLecturerAvailables.Add(newLecturerAvailable);
        }
    }
}
