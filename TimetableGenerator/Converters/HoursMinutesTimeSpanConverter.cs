﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TimetableGenerator.Converters
{
    public class HoursMinutesTimeSpanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
                              CultureInfo culture)
        {
            if (value != null)
            {
                return ((TimeSpan)value).ToString(@"hh\:mm");
            }
            else
            {
                return new TimeSpan();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter,
                                  CultureInfo culture)
        {
            if (value.ToString() == null)
            {
                return null;
            }
            return TimeSpan.ParseExact(value.ToString(), @"hh\:mm", CultureInfo.CurrentCulture);
        }
    }
}
