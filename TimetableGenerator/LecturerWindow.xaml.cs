﻿using BusinessLogic.DataMangement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimetableGenerator.ViewModels;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for LecturerWindow.xaml
    /// </summary>
    public partial class LecturerWindow : Window
    {
        private LecturerViewModel lecturerViewModel;
        private LecturerManagement lecturerManagement;

        public LecturerWindow(Data.TimetableGeneratorContext entities)
        {
            lecturerViewModel = new LecturerViewModel();
            lecturerManagement = new LecturerManagement(entities);
            lecturerViewModel.Lecturers = lecturerManagement.GetAllLecturer();
            DataContext = lecturerViewModel;
            InitializeComponent();
        }

        private void DoubleClick_ModifyLecturer(object sender, MouseButtonEventArgs e)
        {
            if (lecturerViewModel.SelectedLecturer != null)
            {
                new LecturerEditorWindow(lecturerViewModel, lecturerManagement, true).ShowDialog();
            }
        }

        private void Click_NewLecturer(object sender, RoutedEventArgs e)
        {
            new LecturerEditorWindow(lecturerViewModel, lecturerManagement, false).ShowDialog();
        }

        private void Click_DeleteLecturer(object sender, RoutedEventArgs e)
        {
            if (lecturerViewModel.SelectedLecturer != null)
            {
                lecturerManagement.DeleteLecturer(lecturerViewModel.SelectedLecturer);
                lecturerViewModel.Lecturers.Remove(lecturerViewModel.SelectedLecturer);
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva tanár.", "Tanár törlés.", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
