﻿using BusinessLogic.DataMangement;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimetableGenerator.ViewModels;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for CourseWindow.xaml
    /// </summary>
    public partial class CourseEditorWindow : Window
    {
        private SubjectViewModel subjectViewModel;
        private CourseManagement courseManagement;
        private bool modify;
        private ObservableCollection<Course> Courses;

        public CourseEditorWindow(SubjectViewModel subjectViewModel, CourseManagement courseManagement, bool modify)
        {
            this.subjectViewModel = subjectViewModel;
            this.courseManagement = courseManagement;
            this.modify = modify;
            DataContext = this.subjectViewModel;
            this.subjectViewModel.EditorCourse = new Course();
            if (this.modify)
            {
                this.subjectViewModel.EditorCourse.CourseId = this.subjectViewModel.SelectedCourse.CourseId;
                this.subjectViewModel.EditorCourse.Name = this.subjectViewModel.SelectedCourse.Name;
                this.subjectViewModel.EditorCourse.HeadCount = this.subjectViewModel.SelectedCourse.HeadCount;
                this.subjectViewModel.EditorCourse.NumberOfClasses = this.subjectViewModel.SelectedCourse.NumberOfClasses;
                this.subjectViewModel.EditorCourse.StartTime = this.subjectViewModel.SelectedCourse.StartTime;
                this.subjectViewModel.EditorCourse.Day = this.subjectViewModel.SelectedCourse.Day;
                this.subjectViewModel.EditorCourse.Type = this.subjectViewModel.SelectedCourse.Type;
                this.subjectViewModel.EditorCourse.SubjectId = this.subjectViewModel.SelectedCourse.SubjectId;
                subjectViewModel.EditorRoomCourseConnects = new ObservableCollection<RoomCourseConnect>(subjectViewModel.SelectedCourse.RoomCourseConnect);
            }
            else
            {
                subjectViewModel.EditorRoomCourseConnects = new ObservableCollection<RoomCourseConnect>();
            }
            if (subjectViewModel.SelectedSubject.Course != null)
                Courses = new ObservableCollection<Course>(subjectViewModel.SelectedSubject.Course);
            else
                Courses = new ObservableCollection<Course>();

            subjectViewModel.Rooms = courseManagement.GetAllRooms();

            InitializeComponent();
        }

        private void Click_SaveCourse(object sender, RoutedEventArgs e)
        {
            
            if (!courseManagement.CheckCourseNumberOfClasses(subjectViewModel.EditorCourse))
            {
                MessageBox.Show("A kurzus nem tarthat ilyen sokáig.", "Óraszám hiba!", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }
            if (!courseManagement.CheckRoomCourseConnectHeadCount(subjectViewModel.EditorCourse ,subjectViewModel.EditorRoomCourseConnects))
            {
                MessageBox.Show("A hozzárendelt tantermek olyan termet tartalmaz, amelynek a férőhelye kisebb mint a kurzus létszáma", "Létszám hiba!", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }
            if (!courseManagement.CheckRoomCourseConnectTypes(subjectViewModel.EditorCourse, subjectViewModel.EditorRoomCourseConnects))
            {
                MessageBox.Show("A hozzárendelt tantermek olyan termet tartalmaz, amely nem felel meg a kurzus típusának.", "Típus hiba!", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }
            if (!courseManagement.CheckCourseHeadCount(subjectViewModel.EditorCourse))
            {
                MessageBox.Show("Nincs olyan terem amibe elférne a kurzus.", "Létszám hiba!", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }

            subjectViewModel.EditorCourse.RoomCourseConnect = subjectViewModel.EditorRoomCourseConnects;
            subjectViewModel.EditorCourse.SubjectId = subjectViewModel.SelectedSubject.SubjectId;
            if (!modify)
            {
                subjectViewModel.Courses.Add(subjectViewModel.EditorCourse);
                courseManagement.AddNewCourse(subjectViewModel.EditorCourse);
            }
            else
            {
                courseManagement.ModifyCourse(subjectViewModel.SelectedCourse.CourseId, subjectViewModel.EditorCourse);
                int index = Courses.IndexOf(subjectViewModel.SelectedCourse);
                Courses[index] = subjectViewModel.SelectedCourse;
                subjectViewModel.SelectedSubject.Course = Courses;
            }
            subjectViewModel.SelectedCourse = null;
            this.Close();
        }

        private void Click_CancelCourse(object sender, RoutedEventArgs e)
        {
            subjectViewModel.EditorCourse = null;
            this.Close();
        }

        private void Click_AddConnect(object sender, RoutedEventArgs e)
        {
            if (subjectViewModel.SelectedRoom != null)
            {
                if (!courseManagement.CheckRoomType(subjectViewModel.EditorCourse,subjectViewModel.SelectedRoom))
                {
                    MessageBox.Show(String.Format("A kurzushoz nem adható csak {0} típusú tanterem.", EnumExtension.GetDescription(subjectViewModel.EditorCourse.Type)), "Tanterem hozzáadás hiba!", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                    return;
                }

                IEnumerator<RoomCourseConnect> enumerator = subjectViewModel.EditorRoomCourseConnects.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.RoomId == subjectViewModel.SelectedRoom.RoomId)
                        return;
                }

                RoomCourseConnect newRoomCourseConnect = new RoomCourseConnect() { Room = subjectViewModel.SelectedRoom };
                subjectViewModel.EditorRoomCourseConnects.Add(newRoomCourseConnect);
            }
        }

        private void Click_DeleteConnect(object sender, RoutedEventArgs e)
        {
            if (subjectViewModel.SelectedRoomCourseConnect != null)
            {
                subjectViewModel.EditorRoomCourseConnects.Remove(subjectViewModel.SelectedRoomCourseConnect);
                subjectViewModel.SelectedRoomCourseConnect = null;
            }
        }

        private void DoubleClick_AddConnect(object sender, MouseButtonEventArgs e)
        {
            if (subjectViewModel.SelectedRoom != null)
            {
                if (!courseManagement.CheckRoomType(subjectViewModel.EditorCourse, subjectViewModel.SelectedRoom))
                {
                    MessageBox.Show(String.Format("A kurzushoz nem adható csak {0} típusú tanterem.", EnumExtension.GetDescription(subjectViewModel.EditorCourse.Type)), "Tanterem hozzáadás hiba!", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                    return;
                }
                IEnumerator<RoomCourseConnect> enumerator = subjectViewModel.EditorRoomCourseConnects.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.Room.RoomId == subjectViewModel.SelectedRoom.RoomId)
                        return;
                }

                RoomCourseConnect newRoomCourseConnect = new RoomCourseConnect() { Room = subjectViewModel.SelectedRoom };
                subjectViewModel.EditorRoomCourseConnects.Add(newRoomCourseConnect);
            }
        }

        private void DoubleClick_RemoveConnect(object sender, MouseButtonEventArgs e)
        {
                subjectViewModel.EditorRoomCourseConnects.Remove(subjectViewModel.SelectedRoomCourseConnect);
                subjectViewModel.SelectedRoomCourseConnect = null;
        }
    }
}
