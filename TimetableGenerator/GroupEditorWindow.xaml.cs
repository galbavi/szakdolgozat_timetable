﻿using BusinessLogic.DataMangement;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimetableGenerator.ViewModels;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for GroupEditorWindow.xaml
    /// </summary>
    public partial class GroupEditorWindow : Window
    {
        GroupViewModel groupViewModel;
        GroupManagement groupManagement;
        bool modify;

        public GroupEditorWindow(GroupViewModel groupViewModel, GroupManagement groupManagement, bool modify)
        {
            this.groupViewModel = groupViewModel;
            this.groupManagement = groupManagement;
            this.modify = modify;
            DataContext = this.groupViewModel;
            groupViewModel.Subjects = groupManagement.GetAllSubject();
            this.groupViewModel.EditorGroupHeader = new GroupHeader();
            if (this.modify)
            {
                this.groupViewModel.EditorGroupHeader.GroupHeaderId = this.groupViewModel.SelectedGroupHeader.GroupHeaderId;
                this.groupViewModel.EditorGroupHeader.Name = this.groupViewModel.SelectedGroupHeader.Name;
                this.groupViewModel.EditorGroupLines = new ObservableCollection<GroupLine>(groupViewModel.SelectedGroupHeader.GroupLine);
            }
            else
            {
                groupViewModel.EditorGroupLines = new ObservableCollection<GroupLine>();
            }
            groupViewModel.Subjects = groupManagement.GetAllSubject();

            InitializeComponent();
        }

        private void Click_SaveGroup(object sender, RoutedEventArgs e)
        {
            groupViewModel.EditorGroupHeader.GroupLine = groupViewModel.EditorGroupLines;
            if (!modify)
            {
                groupViewModel.GroupHeaders.Add(groupViewModel.EditorGroupHeader);
                groupManagement.AddNewGroup(groupViewModel.EditorGroupHeader);
            }
            else
            {
                groupManagement.ModifyGroup(groupViewModel.SelectedGroupHeader.GroupHeaderId, groupViewModel.EditorGroupHeader);
                int index = groupViewModel.GroupHeaders.IndexOf(groupViewModel.SelectedGroupHeader);
                groupViewModel.GroupHeaders[index] = groupViewModel.EditorGroupHeader;
            }
            groupViewModel.SelectedGroupHeader = groupViewModel.EditorGroupHeader;
            this.Close();
        }

        private void Click_CancelGroup(object sender, RoutedEventArgs e)
        {
            groupViewModel.EditorGroupHeader = null;
            this.Close();
        }

        private void Click_AddSubject(object sender, RoutedEventArgs e)
        {
            if (groupViewModel.SelectedSubject != null)
            {
                IEnumerator<GroupLine> enumerator = groupViewModel.EditorGroupLines.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.SubjectId == groupViewModel.SelectedSubject.SubjectId)
                        return;
                }

                GroupLine newGroupLine = new GroupLine() { Subject = groupViewModel.SelectedSubject };
                groupViewModel.EditorGroupLines.Add(newGroupLine);
            }
        }

        private void Click_DeleteSubject(object sender, RoutedEventArgs e)
        {
            if (groupViewModel.SelectedGroupLine != null)
            {
                groupViewModel.EditorGroupLines.Remove(groupViewModel.SelectedGroupLine);
                groupViewModel.SelectedGroupLine = null;
            }
        }

        private void DoubleClick_AddSubject(object sender, MouseButtonEventArgs e)
        {
            if (groupViewModel.SelectedSubject != null)
            {
                IEnumerator<GroupLine> enumerator = groupViewModel.EditorGroupLines.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.SubjectId == groupViewModel.SelectedSubject.SubjectId)
                        return;
                }

                GroupLine newGroupLine = new GroupLine() { Subject = groupViewModel.SelectedSubject };
                groupViewModel.EditorGroupLines.Add(newGroupLine);
            }
        }

        private void DoubleClick_DeleteSubject(object sender, MouseButtonEventArgs e)
        {
            if (groupViewModel.SelectedGroupLine != null)
            {
                groupViewModel.EditorGroupLines.Remove(groupViewModel.SelectedGroupLine);
                groupViewModel.SelectedGroupLine = null;
            }
        }
    }
}
