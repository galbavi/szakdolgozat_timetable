﻿using BusinessLogic.DataMangement;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimetableGenerator.ViewModels;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for RoomEditorWindow.xaml
    /// </summary>
    public partial class RoomEditorWindow : Window
    {
        private RoomViewModel roomViewModel;
        private RoomManagement roomManagement;
        private bool modify;

        public RoomEditorWindow(RoomViewModel roomViewModel, RoomManagement roomManagement, bool modify)
        {
            this.roomViewModel = roomViewModel;
            this.roomManagement = roomManagement;
            this.modify = modify;
            DataContext = this.roomViewModel;
            this.roomViewModel.EditorRoom = new Room();
            if (this.modify)
            {
                this.roomViewModel.EditorRoom.Name = this.roomViewModel.SelectedRoom.Name;
                this.roomViewModel.EditorRoom.HeadCount = this.roomViewModel.SelectedRoom.HeadCount;
                this.roomViewModel.EditorRoom.RoomId = this.roomViewModel.SelectedRoom.RoomId;
                this.roomViewModel.EditorRoomTypes = new ObservableCollection<RoomType>(this.roomViewModel.SelectedRoom.RoomType);
            }
            else
            {
                this.roomViewModel.EditorRoomTypes = new ObservableCollection<RoomType>();
            }
            InitializeComponent();
        }

        private void Click_AddCourseType(object sender, RoutedEventArgs e)
        {
            IEnumerator<RoomType> enumerator = roomViewModel.EditorRoomTypes.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Type == roomViewModel.SelectedCourseType)
                    return;
            }

            RoomType newRoomType = new RoomType() { Type = roomViewModel.SelectedCourseType};
            roomViewModel.EditorRoomTypes.Add(newRoomType);
        }

        private void Click_DeleteCourseType(object sender, RoutedEventArgs e)
        {
            roomViewModel.EditorRoomTypes.Remove(roomViewModel.SelectedRoomType);
        }

        private void Click_DeleteAllCourseType(object sender, RoutedEventArgs e)
        {
            roomViewModel.EditorRoomTypes.Clear();
        }

        private void Click_SaveRoom(object sender, RoutedEventArgs e)
        {
            if (!roomManagement.CheckRoomTypes(roomViewModel.EditorRoomTypes))
            {
                MessageBox.Show("Legalább egy típust kötelező megadni.", "Típus hiba!", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }
            if(!roomManagement.CheckCourseHeadCounts(roomViewModel.EditorRoom))
            {
                MessageBox.Show("A létszám nem megfelelő, mert a tanterem már hozzá van rendelve, olyan kurzushoz, amelynek magasabb a létszáma.", "Létszám hiba!", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }

            roomViewModel.EditorRoom.RoomType = roomViewModel.EditorRoomTypes;
            if (!modify)
            {
                roomViewModel.Rooms.Add(roomViewModel.EditorRoom);
                roomManagement.AddNewRoom(roomViewModel.EditorRoom);
            }
            else
            {
                roomManagement.ModifyRoom(roomViewModel.SelectedRoom.RoomId, roomViewModel.EditorRoom);
                int index = roomViewModel.Rooms.IndexOf(roomViewModel.SelectedRoom);
                roomViewModel.Rooms[index] = roomViewModel.EditorRoom;
            }
            roomViewModel.SelectedRoom = roomViewModel.EditorRoom;
            this.Close();
        }

        private void Click_CloseRoom(object sender, RoutedEventArgs e)
        {
            roomViewModel.EditorRoom = null;
            this.Close();
        }
    }
}
