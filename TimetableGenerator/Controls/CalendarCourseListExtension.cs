﻿using BusinessLogic.CalendarModel;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimetableGenerator.Controls
{
    public static class CalendarCourseListExtension
    {
        public static IEnumerable<CalendarCourse> GetByDay(this IEnumerable<CalendarCourse> calendarCourses, WeekDay day)
        {
            var app = from a in calendarCourses
                      where a.WeekDay == day
                      select a;
            return app;
        }
    }
}
