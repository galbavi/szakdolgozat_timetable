﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TimetableGenerator.Controls
{
    class TimeSlotPanel : Panel
    {
        public static readonly DependencyProperty StartTimeProperty =
            DependencyProperty.RegisterAttached("StartTime", typeof(TimeSpan), typeof(TimeSlotPanel),
                new FrameworkPropertyMetadata(DateTime.Now.TimeOfDay));

        public static TimeSpan GetStartTime(DependencyObject d)
        {
            return (TimeSpan)d.GetValue(StartTimeProperty);
        }
        public static void SetStartTime(DependencyObject d, DateTime value)
        {
            d.SetValue(StartTimeProperty, value);
        }

        public static readonly DependencyProperty EndTimeProperty =
            DependencyProperty.RegisterAttached("EndTime", typeof(TimeSpan), typeof(TimeSlotPanel),
                new FrameworkPropertyMetadata(DateTime.Now.TimeOfDay));

        public static TimeSpan GetEndTime(DependencyObject d)
        {
            return (TimeSpan)d.GetValue(EndTimeProperty);
        }

        public static void SetEndTime(DependencyObject d, DateTime value)
        {
            d.SetValue(EndTimeProperty, value);
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            Size size = new Size(double.PositiveInfinity, double.PositiveInfinity);

            foreach (UIElement element in this.Children)
            {
                element.Measure(size);
            }

            return new Size();
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            double top = 0;
            double left = 0;
            double width = 0;
            double height = 0;

            foreach (UIElement element in this.Children)
            {
                Nullable<TimeSpan> startTime = element.GetValue(TimeSlotPanel.StartTimeProperty) as Nullable<TimeSpan>;
                Nullable<TimeSpan> endTime = element.GetValue(TimeSlotPanel.EndTimeProperty) as Nullable<TimeSpan>;

                double start_minutes = (startTime.Value.Hours * 60) + startTime.Value.Minutes;
                double end_minutes = (endTime.Value.Hours * 60) + endTime.Value.Minutes;
                double start_offset = (finalSize.Height / (24 * 60)) * start_minutes;
                double end_offset = (finalSize.Height / (24 * 60)) * end_minutes;

                top = start_offset + 1;

                width = finalSize.Width;
                height = (end_offset - start_offset) - 2;

                element.Arrange(new Rect(left, top, width, height));
            }

            return finalSize;
        }
    }
}
