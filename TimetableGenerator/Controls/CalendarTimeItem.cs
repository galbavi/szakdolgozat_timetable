﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TimetableGenerator.Controls
{
    class CalendarTimeItem : Control
    {
        static CalendarTimeItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CalendarTimeItem), new FrameworkPropertyMetadata(typeof(CalendarTimeItem)));
        }

        public static readonly DependencyProperty TimeslotHourProperty =
            DependencyProperty.Register("TimeslotHour", typeof(string), typeof(CalendarTimeItem),
                new FrameworkPropertyMetadata((string)string.Empty));

        public string TimeslotHour
        {
            get { return (string)GetValue(TimeslotHourProperty); }
            set { SetValue(TimeslotHourProperty, value); }
        }

        public static readonly DependencyProperty TimeslotMinuteProperty =
           DependencyProperty.Register("TimeslotMinute", typeof(string), typeof(CalendarTimeItem),
               new FrameworkPropertyMetadata((string)string.Empty));

        public string TimeslotMinute
        {
            get { return (string)GetValue(TimeslotMinuteProperty); }
            set { SetValue(TimeslotMinuteProperty, value); }
        }
    }
}
