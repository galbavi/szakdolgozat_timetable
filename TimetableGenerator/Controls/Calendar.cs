﻿using BusinessLogic.CalendarModel;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TimetableGenerator.Controls
{
    class Calendar : Control
    {
        static Calendar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Calendar), new FrameworkPropertyMetadata(typeof(Calendar)));

            CommandManager.RegisterClassCommandBinding(typeof(Calendar), new CommandBinding(NextDay, new ExecutedRoutedEventHandler(OnExecutedNextDay), new CanExecuteRoutedEventHandler(OnCanExecuteNextDay)));
            CommandManager.RegisterClassCommandBinding(typeof(Calendar), new CommandBinding(PreviousDay, new ExecutedRoutedEventHandler(OnExecutedPreviousDay), new CanExecuteRoutedEventHandler(OnCanExecutePreviousDay)));
        }

        public static readonly DependencyProperty CalendarCourseListProperty =
           DependencyProperty.Register("CalendarCourseList", typeof(IEnumerable<CalendarCourse>), typeof(Calendar),
           new FrameworkPropertyMetadata(null, new PropertyChangedCallback(Calendar.OnCalendarCourseListChanged)));

        public IEnumerable<CalendarCourse> CalendarCourseList
        {
            get { return (IEnumerable<CalendarCourse>)GetValue(CalendarCourseListProperty); }
            set { SetValue(CalendarCourseListProperty, value); }
        }

        private static void OnCalendarCourseListChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((Calendar)d).OnCalendarCourseListChanged(e);
        }

        protected virtual void OnCalendarCourseListChanged(DependencyPropertyChangedEventArgs e)
        {
            FilterCalendarCourseList();
        }

        public static readonly DependencyProperty CurrentDayProperty =
            DependencyProperty.Register("CurrentDay", typeof(WeekDay), typeof(Calendar),
                new FrameworkPropertyMetadata(WeekDay.Monday,
                    new PropertyChangedCallback(OnCurrentDayChanged)));

        public WeekDay CurrentDay
        {
            get { return (WeekDay)GetValue(CurrentDayProperty); }
            set { SetValue(CurrentDayProperty, value); }
        }

        private static void OnCurrentDayChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((Calendar)d).OnCurrentDayChanged(e);
        }

        protected virtual void OnCurrentDayChanged(DependencyPropertyChangedEventArgs e)
        {
            FilterCalendarCourseList();
        }

        private void FilterCalendarCourseList()
        {
            WeekDay byDay = CurrentDay;
            CalendarDay day = this.GetTemplateChild("day") as CalendarDay;
            day.ItemsSource = CalendarCourseList.GetByDay(byDay);

            TextBlock dayHeader = this.GetTemplateChild("dayHeader") as TextBlock;
            dayHeader.Text = byDay.ToString();
        }

        public static readonly RoutedCommand NextDay = new RoutedCommand("NextDay", typeof(Calendar));
        public static readonly RoutedCommand PreviousDay = new RoutedCommand("PreviousDay", typeof(Calendar));

        private static void OnCanExecuteNextDay(object sender, CanExecuteRoutedEventArgs e)
        {
            ((Calendar)sender).OnCanExecuteNextDay(e);
        }

        private static void OnExecutedNextDay(object sender, ExecutedRoutedEventArgs e)
        {
            ((Calendar)sender).OnExecutedNextDay(e);
        }

        protected virtual void OnCanExecuteNextDay(CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = false;
        }

        protected virtual void OnExecutedNextDay(ExecutedRoutedEventArgs e)
        {
            if (CurrentDay == Enum.GetValues(typeof(WeekDay)).Cast<WeekDay>().Last())
                CurrentDay = Enum.GetValues(typeof(WeekDay)).Cast<WeekDay>().First();
            else
                CurrentDay++;
            e.Handled = true;
        }

        private static void OnCanExecutePreviousDay(object sender, CanExecuteRoutedEventArgs e)
        {
            ((Calendar)sender).OnCanExecutePreviousDay(e);
        }

        private static void OnExecutedPreviousDay(object sender, ExecutedRoutedEventArgs e)
        {
            ((Calendar)sender).OnExecutedPreviousDay(e);
        }

        protected virtual void OnCanExecutePreviousDay(CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = false;
        }

        protected virtual void OnExecutedPreviousDay(ExecutedRoutedEventArgs e)
        {
            if (CurrentDay == Enum.GetValues(typeof(WeekDay)).Cast<WeekDay>().First())
                CurrentDay = Enum.GetValues(typeof(WeekDay)).Cast<WeekDay>().Last();
            else
                CurrentDay--;
            e.Handled = true;
        }

    }
}
