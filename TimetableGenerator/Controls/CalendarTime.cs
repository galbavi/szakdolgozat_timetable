﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TimetableGenerator.Controls
{
    class CalendarTime : Control
    {
        static CalendarTime()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CalendarTime), new FrameworkPropertyMetadata(typeof(CalendarTime)));
        }
    }
}
