﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TimetableGenerator.Controls
{
    class CalendarCourseItem : ContentControl
    {
        static CalendarCourseItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CalendarCourseItem), new FrameworkPropertyMetadata(typeof(CalendarCourseItem)));
        }

        public static readonly DependencyProperty StartTimeProperty =
            TimeSlotPanel.StartTimeProperty.AddOwner(typeof(CalendarCourseItem));

        public bool StartTime
        {
            get { return (bool)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        public static readonly DependencyProperty EndTimeProperty =
            TimeSlotPanel.EndTimeProperty.AddOwner(typeof(CalendarCourseItem));

        public bool EndTime
        {
            get { return (bool)GetValue(EndTimeProperty); }
            set { SetValue(EndTimeProperty, value); }
        }
    }
}
