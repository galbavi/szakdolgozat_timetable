﻿using BusinessLogic.DataMangement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimetableGenerator.ViewModels;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for GroupWindow.xaml
    /// </summary>
    public partial class GroupWindow : Window
    {
        private GroupViewModel groupViewModel;
        private GroupManagement groupManagement;

        public GroupWindow(Data.TimetableGeneratorContext entities)
        {
            groupViewModel = new GroupViewModel();
            groupManagement = new GroupManagement(entities);
            groupViewModel.GroupHeaders = groupManagement.GetAllGroup();
            this.DataContext = groupViewModel;
            InitializeComponent();
        }

        private void Click_DeleteGroup(object sender, RoutedEventArgs e)
        {
            if (groupViewModel.SelectedGroupHeader != null)
            {
                groupManagement.DeleteGroup(groupViewModel.SelectedGroupHeader);
                groupViewModel.GroupHeaders.Remove(groupViewModel.SelectedGroupHeader);
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva csoport.", "Csoport törlés.", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Click_NewGroup(object sender, RoutedEventArgs e)
        {
            new GroupEditorWindow(groupViewModel, groupManagement, false).ShowDialog();
        }

        private void Click_DeleteAllGroup(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Biztos törli az összes csoportot?", "Összes törlése", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                groupManagement.DeleteAllGroup(groupViewModel.GroupHeaders);
                groupViewModel.GroupHeaders.Clear();
            }
        }

        private void DoubleClick_ModifyGroup(object sender, MouseButtonEventArgs e)
        {
            new GroupEditorWindow(groupViewModel, groupManagement, true).ShowDialog();
        }
    }
}
