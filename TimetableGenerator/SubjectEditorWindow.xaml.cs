﻿using BusinessLogic.DataMangement;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimetableGenerator.ViewModels;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for SubjectEditorWindow.xaml
    /// </summary>
    public partial class SubjectEditorWindow : Window
    {
        SubjectViewModel subjectViewModel;
        SubjectManagement subjectManagement;
        bool modify;

        public SubjectEditorWindow(SubjectViewModel subjectViewModel, SubjectManagement subjectManagement, bool modify)
        {
            this.subjectViewModel = subjectViewModel;
            this.subjectManagement = subjectManagement;
            this.modify = modify;
            DataContext = this.subjectViewModel;
            this.subjectViewModel.EditorSubject = new Subject();
            if(this.modify)
            {
                this.subjectViewModel.EditorSubject.SubjectId = this.subjectViewModel.SelectedSubject.SubjectId;
                this.subjectViewModel.EditorSubject.Name = this.subjectViewModel.SelectedSubject.Name;
                subjectViewModel.EditorSubjectLecturerConnects = new ObservableCollection<SubjectLecturerConnect>(subjectViewModel.SelectedSubject.SubjectLecturerConnect);
            }
            else
            {
                subjectViewModel.EditorSubjectLecturerConnects = new ObservableCollection<SubjectLecturerConnect>();
            }
            subjectViewModel.Lecturers = subjectManagement.GetAllLecturer();

            InitializeComponent();
        }

        private void Click_AddConnect(object sender, RoutedEventArgs e)
        {
            if (subjectViewModel.SelectedLecturer != null)
            {
                IEnumerator<SubjectLecturerConnect> enumerator = subjectViewModel.EditorSubjectLecturerConnects.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.LecturerId == subjectViewModel.SelectedLecturer.LecturerId)
                        return;
                }

                SubjectLecturerConnect newSubjectLecturerConnect = new SubjectLecturerConnect() { Lecturer = subjectViewModel.SelectedLecturer };
                subjectViewModel.EditorSubjectLecturerConnects.Add(newSubjectLecturerConnect);
            }
        }

        private void Click_DeleteConnect(object sender, RoutedEventArgs e)
        {
            if (subjectViewModel.SelectedSubjectLecturerConnect != null)
            {
                subjectViewModel.EditorSubjectLecturerConnects.Remove(subjectViewModel.SelectedSubjectLecturerConnect);
                subjectViewModel.SelectedSubjectLecturerConnect = null;
            }
        }

        private void Click_CancelSubject(object sender, RoutedEventArgs e)
        {
            subjectViewModel.EditorSubject = null;
            this.Close();
        }

        private void Click_SaveSubject(object sender, RoutedEventArgs e)
        {
            subjectViewModel.EditorSubject.SubjectLecturerConnect = subjectViewModel.EditorSubjectLecturerConnects;

            if (!modify)
            {
                subjectViewModel.Subjects.Add(subjectViewModel.EditorSubject);
                subjectManagement.AddNewSubject(subjectViewModel.EditorSubject);
            }
            else
            {
                subjectManagement.ModifySubject(subjectViewModel.SelectedSubject.SubjectId, subjectViewModel.EditorSubject);
                subjectViewModel.EditorSubject.Course = subjectViewModel.SelectedSubject.Course;
                int index = subjectViewModel.Subjects.IndexOf(subjectViewModel.SelectedSubject);
                subjectViewModel.Subjects[index] = subjectViewModel.EditorSubject;
            }
            subjectViewModel.SelectedSubject = subjectViewModel.EditorSubject;
            this.Close();
        }

        private void DoubleClick_AddConnect(object sender, MouseButtonEventArgs e)
        {
            if (subjectViewModel.SelectedLecturer != null)
            {
                IEnumerator<SubjectLecturerConnect> enumerator = subjectViewModel.EditorSubjectLecturerConnects.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.Lecturer.LecturerId == subjectViewModel.SelectedLecturer.LecturerId)
                        return;
                }

                SubjectLecturerConnect newSubjectLecturerConnect = new SubjectLecturerConnect() { Lecturer = subjectViewModel.SelectedLecturer };
                subjectViewModel.EditorSubjectLecturerConnects.Add(newSubjectLecturerConnect);
            }
        }

        private void DoubleClick_RemoveConnect(object sender, MouseButtonEventArgs e)
        {
            if (subjectViewModel.SelectedSubjectLecturerConnect != null)
            {
                subjectViewModel.EditorSubjectLecturerConnects.Remove(subjectViewModel.SelectedSubjectLecturerConnect);
                subjectViewModel.SelectedSubjectLecturerConnect = null;
            }
        }
    }
}
