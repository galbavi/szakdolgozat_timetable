﻿using BusinessLogic.DataMangement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimetableGenerator.ViewModels;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for RoomWindow.xaml
    /// </summary>
    public partial class RoomWindow : Window
    {
        private RoomViewModel roomViewModel;
        private RoomManagement roomManagement;

        public RoomWindow(Data.TimetableGeneratorContext entities)
        {
            roomViewModel = new RoomViewModel();
            roomManagement = new RoomManagement(entities);
            roomViewModel.Rooms = roomManagement.GetAllRoom();
            this.DataContext = roomViewModel;
            InitializeComponent();
        }

        private void DoubleClick_ModifyRoom(object sender, MouseButtonEventArgs e)
        {
            if (roomViewModel.SelectedRoom != null)
            {
                new RoomEditorWindow(roomViewModel, roomManagement, true).ShowDialog();
            }
        }

        private void Click_NewRoom(object sender, RoutedEventArgs e)
        {
            new RoomEditorWindow(roomViewModel, roomManagement, false).ShowDialog();
        }

        private void Click_DeleteRoom(object sender, RoutedEventArgs e)
        {
            if(roomViewModel.SelectedRoom != null)
            {
                roomManagement.DeleteRoom(roomViewModel.SelectedRoom);
                roomViewModel.Rooms.Remove(roomViewModel.SelectedRoom);
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva tanterem.", "Tanterem törlés.", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
