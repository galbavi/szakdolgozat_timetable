﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Data.Repositories;
using Data;
using Data.Interfaces;
using Data.Models;
using TimetableGenerator.ViewModels;
using BusinessLogic.DataMangement;
using BusinessLogic.CalendarModel;

namespace TimetableGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private TimetableGeneratorContext entities = new TimetableGeneratorContext();
        private MainViewModel mainViewModel;
        private TimetableManagement timetableManagement;
        public MainWindow()
        {
            mainViewModel = new MainViewModel();
            timetableManagement = new TimetableManagement(entities);
            mainViewModel.Timetables = timetableManagement.GetAllTimetable();
            DataContext = mainViewModel;
        }

        private void Click_RoomManagement(object sender, RoutedEventArgs e)
        {
            new RoomWindow(entities).ShowDialog();
        }

        private void Click_SubjectManagement(object sender, RoutedEventArgs e)
        {
            new SubjectWindow(entities).ShowDialog();
        }

        private void Click_LecturerManagement(object sender, RoutedEventArgs e)
        {
            new LecturerWindow(entities).ShowDialog();
        }

        private void Click_GroupManagement(object sender, RoutedEventArgs e)
        {
            new GroupWindow(entities).ShowDialog();
        }

        private void Click_GenerateNewTimeTable(object sender, RoutedEventArgs e)
        {
            if (mainViewModel.NewTimetableName != null && mainViewModel.NewTimetableName != string.Empty)
            {
                if (MessageBox.Show("Az órarend generálás hosszab időbe telhet. Folytatja?", "Órarend generálása", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                {
                    mainViewModel.Timetables.Add(timetableManagement.GenerateNewTimetable(mainViewModel.NewTimetableName));
                    MessageBox.Show("Az órarend elkészült", "Kész", MessageBoxButton.OK, MessageBoxImage.Information);
                }
               
            }
            else
                MessageBox.Show("Név kitöltése kötelező.", "Hiba", MessageBoxButton.OK, MessageBoxImage.Error);

        }

        private void Click_DeleteTimetable(object sender, RoutedEventArgs e)
        {
            if (mainViewModel.SelectedTimeTable != null)
            {
                timetableManagement.DeleteTimetable(mainViewModel.SelectedTimeTable);
                mainViewModel.Timetables.Remove(mainViewModel.SelectedTimeTable);
                mainViewModel.SelectedTimeTable = null;
            }
        }

        private void DoubleClick_OpenTimetableCalendar(object sender, MouseButtonEventArgs e)
        {
            if (mainViewModel.SelectedTimeTable != null)
            {
                new CalendarWindow(timetableManagement.GetCoursesFromTimetableToCalendar(mainViewModel.SelectedTimeTable)).Show();
            }
        }

        private void Click_ExportToExcel(object sender, RoutedEventArgs e)
        {
            if (mainViewModel.SelectedTimeTable != null)
            {
                System.Windows.Forms.FolderBrowserDialog fldDialog = new System.Windows.Forms.FolderBrowserDialog();
                System.Windows.Forms.DialogResult fldResult = fldDialog.ShowDialog();
                if (fldResult == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fldDialog.SelectedPath))
                {
                    timetableManagement.ExportToExcel(fldDialog.SelectedPath, mainViewModel.SelectedTimeTable);
                }
            }

        }
    }
}
