﻿using BusinessLogic.CalendarModel;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimetableGenerator.ViewModels
{
    public class RoomViewModel : Bindable
    {
        private Room selectedRoom;

        public RoomViewModel()
        {
            Rooms = new ObservableCollection<Room>();
            EditorRoomTypes = new ObservableCollection<RoomType>();
        }
        public Room EditorRoom { get; set; }

        public ObservableCollection<RoomType> EditorRoomTypes { get; set; }

        public Room SelectedRoom
        {
            get
            {
                return selectedRoom;
            } 
            set
            {
                selectedRoom = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Room> Rooms { get; set; }

        public RoomType SelectedRoomType { get; set; }

        public Array CourseTypes
        {
            get
            {
                return Enum.GetValues(typeof(CourseType));
            }
        }

        public CourseType SelectedCourseType { get; set; }
    }
}
