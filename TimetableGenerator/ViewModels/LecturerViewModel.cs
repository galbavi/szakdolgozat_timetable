﻿using BusinessLogic.CalendarModel;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimetableGenerator.ViewModels
{
    public class LecturerViewModel : Bindable
    {
        private Lecturer selectedLecturer;

        public LecturerViewModel()
        {

        }

        public Lecturer EditorLecturer { get; set; }

        public ObservableCollection<LecturerAvailable> EditorLecturerAvailables { get; set; }

        public ObservableCollection<LecturerType> EditorLecturerTypes { get; set; }

        public Lecturer SelectedLecturer
        {
            get
            {
                return selectedLecturer;
            }
            set
            {
                selectedLecturer = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Lecturer> Lecturers { get; set; }

        public LecturerType SelectedLecturerType { get; set; }

        public LecturerAvailable SelectedLecturerAvailable { get; set; }

        public Array CourseTypes
        {
            get
            {
                return Enum.GetValues(typeof(CourseType));
            }
        }

        public CourseType SelectedCourseType { get; set; }

        public Array WeekDays
        {
            get
            {
                return Enum.GetValues(typeof(WeekDay));
            }
        }

        public WeekDay SelectedWeekDay { get; set; }

        public string EditorStartTime { get; set; }

        public string EditorEndTime { get; set; }
    }
}
