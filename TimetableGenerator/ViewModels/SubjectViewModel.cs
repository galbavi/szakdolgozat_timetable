﻿using BusinessLogic.CalendarModel;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimetableGenerator.ViewModels
{
    public class SubjectViewModel : Bindable
    {
        private Subject selectedSubject;
        private Course selectedCourse;

        public SubjectViewModel()
        {
            Courses = new ObservableCollection<Course>();
            Subjects = new ObservableCollection<Subject>();
        }

        #region Subject Editor

        public Subject EditorSubject { get; set; }

        public ObservableCollection<SubjectLecturerConnect> EditorSubjectLecturerConnects { get; set; }

        public ObservableCollection<Lecturer> Lecturers { get; set; }

        public Lecturer SelectedLecturer { get; set; }

        public SubjectLecturerConnect SelectedSubjectLecturerConnect { get; set; }
        

        #endregion Subject Editor

        #region Course Editor

        public Course EditorCourse { get; set; }

        public ObservableCollection<RoomCourseConnect> EditorRoomCourseConnects { get; set; }

        public ObservableCollection<Room> Rooms { get; set; }

        public Array CourseTypes
        {
            get
            {
                return Enum.GetValues(typeof(CourseType));
            }
        }

        public Array WeekDays
        {
            get
            {
                return Enum.GetValues(typeof(WeekDay));
            }
        }

        public WeekDay SelectedWeekDay { get; set; }

        public string StartTime { get; set; }

        public Room SelectedRoom { get; set; }

        public RoomCourseConnect SelectedRoomCourseConnect { get; set; }

        #endregion Course Editor

        public Subject SelectedSubject
        {
            get
            {
                return selectedSubject;
            }
            set
            {
                selectedSubject = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Subject> Subjects { get; set; }

        public Course SelectedCourse {
            get
            {
                return selectedCourse;
            }
            set
            {
                selectedCourse = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Course> Courses { get; internal set; }
    }
}
