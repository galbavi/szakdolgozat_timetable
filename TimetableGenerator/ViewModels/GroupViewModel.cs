﻿using BusinessLogic.CalendarModel;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimetableGenerator.ViewModels
{
    public class GroupViewModel : Bindable
    {
        private GroupHeader selectedGroupHeader;

        public GroupViewModel()
        {
            EditorGroupLines = new ObservableCollection<GroupLine>();
            GroupHeaders = new ObservableCollection<GroupHeader>();
        }

        public GroupHeader EditorGroupHeader { get; set; }

        public ObservableCollection<GroupLine> EditorGroupLines { get; set; }

        public GroupLine SelectedGroupLine { get; set; }

        public ObservableCollection<Subject> Subjects { get; set; }

        public Subject SelectedSubject { get; set; }

        public GroupHeader SelectedGroupHeader
        {
            get
            {
                return selectedGroupHeader;
            }
            set
            {
                selectedGroupHeader = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<GroupHeader> GroupHeaders { get; set; }
    }
}
