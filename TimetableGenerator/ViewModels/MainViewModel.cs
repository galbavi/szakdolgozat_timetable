﻿using BusinessLogic.CalendarModel;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimetableGenerator.ViewModels
{
    class MainViewModel : Bindable
    {
        public MainViewModel()
        {
            Timetables = new ObservableCollection<TimetableHeader>();
        }

        public string NewTimetableName { get; set; }
    
        public ObservableCollection<TimetableHeader> Timetables { get; set; }

        public TimetableHeader SelectedTimeTable { get; set; }
    }
}
