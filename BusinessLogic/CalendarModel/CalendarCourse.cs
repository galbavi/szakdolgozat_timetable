﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.CalendarModel
{
    public class CalendarCourse : Bindable
    {
        private WeekDay weekDay;
        private TimeSpan startTime;
        private TimeSpan endTime;
        private string courseInformation;

        public TimeSpan StartTime { get => startTime;
            set
            {
                startTime = value;
                OnPropertyChanged();
            }
        }
        public TimeSpan EndTime { get => endTime; set
            {
                endTime = value;
                OnPropertyChanged();
            }
        }
        public string CourseInformation { get => courseInformation; set
            {
                courseInformation = value;
                OnPropertyChanged();
            }
        }

        public WeekDay WeekDay { get => weekDay; set
            {
                weekDay = value;
                OnPropertyChanged();
            }
        }

        public override string ToString()
        {
            return CourseInformation;
        }
    }
}
