﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class GeneratedTimetable
    {
        private List<GeneratedCourse> generatedCourses;
        private double fitness;

        public GeneratedTimetable()
        {
            generatedCourses = new List<GeneratedCourse>();
        }
        public double Fitness { get => fitness; set => fitness = value; }
        public List<GeneratedCourse> GeneratedCourses { get => generatedCourses; set => generatedCourses = value; }
    }
}
