﻿using BusinessLogic.DataMangement;
using BusinessLogic.Exceptions;
using Data;
using Data.Interfaces;
using Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Generator
    {
        private static Random rnd = new Random();
        private static TimeSpan minTime = TimeSpan.FromHours(8);
        private static TimeSpan maxTime = TimeSpan.FromHours(18);
        private static TimeSpan pauseTime = TimeSpan.FromMinutes(10);
        private static int wrongTimeTableMultiplier = 200;
        private static int workTimeOneWeekMinute = 2400;
        private static int oneHourInMinute = 60;
        private static readonly int oneClassMinute = 45;

        private LecturerManagement lecturerManagement;
        private RoomManagement roomManagement;

        private GroupManagement groupManagement;
        private SubjectManagement subjectManagement;
        private CourseManagement courseManagement;

        private List<Lecturer> lecturers;
        private List<Room> rooms;
        private List<GroupHeader> groups;
        private List<Subject> subjects;
        private List<Course> courses;

        private List<GeneratedTimetable> population;
        private GeneratedTimetable p_best;
        private int maxPopulation = 4000;
        private int maxIteration = 2000;
        private int iteration;
        
        private object listLock = new object();

        /// <summary>
        /// Initialite the Generator class.
        /// </summary>
        /// <param name="entities">Database entitis</param>
        public Generator()
        {
        }

        /// <summary>
        /// Generate a new Timetable
        /// </summary>
        /// <param name="newTimetableName">Name of the new timetable</param>
        /// <returns>New timetable</returns>
        public TimetableHeader Generate(string newTimetableName)
        {
            Init();
            LoadData();

            iteration = 0;
            InitializePopulation();
            Evaluation();

            p_best = SelectBest();
            while(!StopCondition())
            {

                List<GeneratedTimetable> next_population = new List<GeneratedTimetable>();
                List<GeneratedTimetable> mating_pool = new List<GeneratedTimetable>();

                SelectParents(population, next_population, mating_pool);
                int diff = population.Count - next_population.Count;
                Parallel.For(0, diff, 
                    index => {
                        List<GeneratedTimetable> selected_parents = Selection(mating_pool);
                        GeneratedTimetable c = Crossover(selected_parents);
                        Mutate(c);
                        lock(listLock)
                        {

                            next_population.Add(c);
                        }
                    });

                population = next_population;
                Evaluation();
                p_best = SelectBest();
                
            }
            
            return CreateNewTimeTable(p_best, newTimetableName);
        }

        private void Init()
        {
            TimetableGeneratorContext entities = new TimetableGeneratorContext();
            lecturerManagement = new LecturerManagement(entities);
            roomManagement = new RoomManagement(entities);
            groupManagement = new GroupManagement(entities);
            subjectManagement = new SubjectManagement(entities);
            courseManagement = new CourseManagement(entities);
        }

        /// <summary>
        /// Create a new timetable
        /// </summary>
        /// <param name="p_best">Best generated timetable</param>
        /// <param name="newTimetableName">Name of the new timetable</param>
        /// <returns>New timetable</returns>
        private TimetableHeader CreateNewTimeTable(GeneratedTimetable p_best, string newTimetableName)
        {
            TimetableHeader newTimetableHeader = new TimetableHeader();
            newTimetableHeader.Name = newTimetableName;
            newTimetableHeader.ProductionDate = DateTime.Now;
            newTimetableHeader.TimetableLine = new List<TimetableLine>();
            foreach (var generatedCourse in p_best.GeneratedCourses)
            {
                TimetableLine newTimetableLine = new TimetableLine();
                Course course = courses.Single(x => x.CourseId == generatedCourse.CourseId);
                Lecturer lecturer = lecturers.Single(x => x.LecturerId == generatedCourse.LecturerId);
                Room room = rooms.Single(x => x.RoomId == generatedCourse.RoomId);
                newTimetableLine.CourseName = course.Name;
                newTimetableLine.Day = generatedCourse.Day;
                newTimetableLine.HeadCount = course.HeadCount;
                newTimetableLine.LecturerName = lecturer.Name;
                newTimetableLine.NumberOfClasses = course.NumberOfClasses;
                newTimetableLine.RoomName = room.Name;
                newTimetableLine.StartTime = generatedCourse.StartTime;
                newTimetableLine.SubjectName = course.Subject.Name;
                newTimetableLine.Type = course.Type;
                newTimetableHeader.TimetableLine.Add(newTimetableLine);
            }
            return newTimetableHeader;
        }

        /// <summary>
        /// Load the datas from the database
        /// </summary>
        private void LoadData()
        {
            lecturers = lecturerManagement.GetAllLecturer().ToList();
            rooms = roomManagement.GetAllRoom().ToList();
            groups = groupManagement.GetAllGroup().ToList();
            subjects = subjectManagement.GetAllSubject().ToList();
            courses = courseManagement.GetAllCourse();
        }

        /// <summary>
        /// Create the population
        /// </summary>
        private void InitializePopulation()
        {
            int i = 0;
            population = new List<GeneratedTimetable>();
            while (i < maxPopulation)
            {
                GeneratedTimetable generatedTimetable = new GeneratedTimetable();
                MakeCourses(generatedTimetable, courses);
                population.Add(generatedTimetable);
                i++;
            }
            
        }

        /// <summary>
        /// Create the genereted courses for the generated timetable
        /// </summary>
        /// <param name="generatedTimetable"></param>
        /// <param name="tempCourses"></param>
        private void MakeCourses(GeneratedTimetable generatedTimetable, List<Course> tempCourses)
        {
            foreach (var course in tempCourses)
            {
                generatedTimetable.GeneratedCourses.Add(MakeNewCoruse(course));
            }
        }

        /// <summary>
        /// Create a new generated course
        /// </summary>
        /// <param name="course">actual course</param>
        /// <returns>Generated course</returns>
        private GeneratedCourse MakeNewCoruse(Course course)
        {
            GeneratedCourse generatedCourse = new GeneratedCourse();
            generatedCourse.SubjectId = course.SubjectId;
            generatedCourse.CourseId = course.CourseId;
            generatedCourse.HeadCount = course.HeadCount;
            generatedCourse.NumberOfClasses = course.NumberOfClasses;
            generatedCourse.CourseType = course.Type;

            SetLecturer(ref generatedCourse, course);
            SetRoom(ref generatedCourse, course);

            if (course.StartTime != null && course.StartTime > TimeSpan.Zero)
            {
                generatedCourse.Day = course.Day.Value;
                generatedCourse.StartTime = course.StartTime.Value;
                generatedCourse.TimeFix = true;
            }
            else
            {
                int time = (int)Math.Round(rnd.Next(minTime.Hours * oneHourInMinute, ((maxTime.Hours * oneHourInMinute) - generatedCourse.NumberOfClasses * 45) + 1) / 5.0) * 5;
                generatedCourse.StartTime = new TimeSpan(time / oneHourInMinute, time % oneHourInMinute, 0);
                generatedCourse.Day = (WeekDay)Enum.ToObject(typeof(WeekDay), rnd.Next((int)WeekDay.Monday, (int)WeekDay.Friday + 1));
            }
            
            return generatedCourse;
        }

        /// <summary>
        /// Set a new room for the generated course
        /// </summary>
        /// <param name="generatedCourse">Generated course</param>
        /// <param name="course">Course</param>
        private void SetRoom(ref GeneratedCourse generatedCourse, Course course)
        {
            if (course.RoomCourseConnect != null && course.RoomCourseConnect.Count > 0)
            {
                var goodRooms = course.RoomCourseConnect.Select(x => x.Room).Where(y => y.RoomType.Select(t => t.Type).Contains(course.Type) && y.HeadCount >= course.HeadCount);
                if (goodRooms == null || goodRooms.Count() == 0)
                    goodRooms = rooms.Where(y => y.RoomType.Select(t => t.Type).Contains(course.Type) && y.HeadCount >= course.HeadCount);
                generatedCourse.RoomId = goodRooms.ElementAt(rnd.Next(0, goodRooms.Count())).RoomId;
            }
            else
            {
                var goodRooms = rooms.Where(y => y.RoomType.Select(t => t.Type).Contains(course.Type) && y.HeadCount >= course.HeadCount);
                generatedCourse.RoomId = goodRooms.ElementAt(rnd.Next(0, goodRooms.Count())).RoomId;
            }
        }

        /// <summary>
        /// Set a new lecturer for the generated course
        /// </summary>
        /// <param name="generatedCourse">Generated course</param>
        /// <param name="course">Course</param>
        private void SetLecturer(ref GeneratedCourse generatedCourse, Course course)
        {
            if (course.Subject.SubjectLecturerConnect != null && course.Subject.SubjectLecturerConnect.Count > 0)
            {
                var goodLecturers = course.Subject.SubjectLecturerConnect.Select(x => x.Lecturer).Where(y => y.LecturerType.Select(t => t.Type).Contains(course.Type));
                if (goodLecturers == null || goodLecturers.Count() == 0)
                    goodLecturers = lecturers.Where(y => y.LecturerType.Select(t => t.Type).Contains(course.Type));
                generatedCourse.LecturerId = goodLecturers.ElementAt(rnd.Next(0, goodLecturers.Count())).LecturerId;
            }
            else
            {
                var goodLecturers = lecturers.Where(y => y.LecturerType.Select(t => t.Type).Contains(course.Type));
                generatedCourse.LecturerId = goodLecturers.ElementAt(rnd.Next(0, goodLecturers.Count())).LecturerId;
            }
        }

        /// <summary>
        /// Check the overlapping between two generated course
        /// </summary>
        /// <param name="generatedCourse1">Generated course</param>
        /// <param name="generatedCourse2">Generated course</param>
        /// <returns>The two generated course is overlap each other or not</returns>
        public bool IsTimeOverlap(GeneratedCourse generatedCourse1, GeneratedCourse generatedCourse2)
        {
            if (generatedCourse1.Day != generatedCourse2.Day)
                return false;

            double course1StartTimeInMinutes = generatedCourse1.StartTime.TotalMinutes - (pauseTime.TotalMinutes / 2);
            double course1EndTimeInMinutes = course1StartTimeInMinutes + (generatedCourse1.NumberOfClasses * oneClassMinute) + (pauseTime.TotalMinutes / 2);
            double course2StartTimeInMinutes = generatedCourse2.StartTime.TotalMinutes - (pauseTime.TotalMinutes / 2);
            double course2EndTimeInMinutes = course2StartTimeInMinutes + (generatedCourse2.NumberOfClasses * oneClassMinute) + (pauseTime.TotalMinutes / 2);

            return course1StartTimeInMinutes < course2EndTimeInMinutes && course2StartTimeInMinutes < course1EndTimeInMinutes;
        }
        
        /// <summary>
        /// Evaulate the population
        /// </summary>
        private void Evaluation()
        {
            for (int i = 0; i < population.Count; i++)
            {
                population[i].Fitness = Fitness(population[i]);
            }
        }

        /// <summary>
        /// Selects the generated timetable with the best fitness
        /// </summary>
        /// <returns>The best generated timetable</returns>
        private GeneratedTimetable SelectBest()
        {
            double fitness = population.Min(x => x.Fitness);
            return population.Find(item => item.Fitness == fitness);
        }

        /// <summary>
        /// Selects the parents for the next population
        /// </summary>
        /// <param name="population">Actual population</param>
        /// <param name="next_population">Next population</param>
        /// <param name="mating_pool">Mating pool</param>
        private void SelectParents(List<GeneratedTimetable> population, List<GeneratedTimetable> next_population, List<GeneratedTimetable> mating_pool)
        {
            population = population.OrderBy(item => item.Fitness).ToList();
            next_population.Add(population.First());
            mating_pool.AddRange(population.GetRange(1, (population.Count / 2)));
        }

        /// <summary>
        /// Selects two parents from the mating pool
        /// </summary>
        /// <param name="mating_pool"></param>
        /// <returns></returns>
        private List<GeneratedTimetable> Selection(List<GeneratedTimetable> mating_pool)
        {
            List<GeneratedTimetable> selected_ps = new List<GeneratedTimetable>();
            for (int i = 0; i < 2; i++)
            {
                selected_ps.Add(mating_pool[rnd.Next(0, mating_pool.Count)]);
            }
            return selected_ps;
        }

        /// <summary>
        /// Create a new generated timetable with crossover.
        /// </summary>
        /// <param name="selected_parents">Parents of the new generated timetable</param>
        /// <returns>New generated timetable</returns>
        private GeneratedTimetable Crossover(List<GeneratedTimetable> selected_parents)
        {
            GeneratedTimetable newGeneratedTimetable = new GeneratedTimetable();
            int startIdx = rnd.Next(0, courses.Count);
            int lastIdx = rnd.Next(startIdx, courses.Count);
            for (int i = 0; i < courses.Count; i++)
            {
                if (i >=startIdx && i <= lastIdx)
                {
                    newGeneratedTimetable.GeneratedCourses.Add(selected_parents[1].GeneratedCourses[i]);
                }
                else
                {
                    newGeneratedTimetable.GeneratedCourses.Add(selected_parents[0].GeneratedCourses[i]);
                }
            }
            return newGeneratedTimetable;
        }

        /// <summary>
        /// Mutate the generated timetable
        /// </summary>
        /// <param name="c">Generated timetable</param>
        private void Mutate(GeneratedTimetable c)
        {
            GeneratedCourse selectedGeneratedCourse = c.GeneratedCourses[rnd.Next(0, c.GeneratedCourses.Count)];
            int randomNumberForMutate = rnd.Next(4);
            switch (randomNumberForMutate)
            {
                case 0:
                    MutateRoom(ref selectedGeneratedCourse);
                    break;
                case 1:
                    MutateTime(ref selectedGeneratedCourse);
                    break;
                case 2:
                    MutateLecturer(ref selectedGeneratedCourse);
                    break;
                case 3:
                    MutateDay(ref selectedGeneratedCourse);
                    break;
            }
        }

        /// <summary>
        /// Mutate the day of the generated course
        /// </summary>
        /// <param name="selectedGeneratedCourse">Selected generated course</param>
        private void MutateDay(ref GeneratedCourse selectedGeneratedCourse)
        {
            if (!selectedGeneratedCourse.TimeFix)
            {
                selectedGeneratedCourse.Day = (WeekDay)Enum.ToObject(typeof(WeekDay), rnd.Next((int)WeekDay.Monday, (int)WeekDay.Friday + 1));
            }
        }

        /// <summary>
        /// Mutate the lecturer of the generated course
        /// </summary>
        /// <param name="selectedGeneratedCourse">Selected generated course</param>
        private void MutateLecturer(ref GeneratedCourse selectedGeneratedCourse)
        {
            GeneratedCourse actGenCourse = selectedGeneratedCourse;
            Course course = courses.Single(x => x.CourseId == actGenCourse.CourseId);
            SetLecturer(ref selectedGeneratedCourse, course);
        }

        /// <summary>
        /// Mutate the Time of the generated course
        /// </summary>
        /// <param name="selectedGeneratedCourse">Selected generated course</param>
        private void MutateTime(ref GeneratedCourse selectedGeneratedCourse)
        {
            if (!selectedGeneratedCourse.TimeFix)
            {
                int time = (int)Math.Round(rnd.Next(minTime.Hours * oneHourInMinute, (maxTime.Hours * oneHourInMinute) - selectedGeneratedCourse.NumberOfClasses * 45) / 5.0) * 5;
                selectedGeneratedCourse.StartTime = new TimeSpan(time / oneHourInMinute, time % oneHourInMinute, 0);
            }
        }

        /// <summary>
        /// Mutate the Room of the generated course
        /// </summary>
        /// <param name="selectedGeneratedCourse">Selected generated course</param>
        private void MutateRoom(ref GeneratedCourse selectedGeneratedCourse)
        {
            GeneratedCourse actGenCourse = selectedGeneratedCourse;
            Course actMutCourse = courses.Single(x => x.CourseId == actGenCourse.CourseId);
            SetRoom(ref selectedGeneratedCourse, actMutCourse);
        }

        private bool StopCondition() { return iteration++ == maxIteration; }

        /// <summary>
        /// Calculate the fitness value of the generated timetable
        /// </summary>
        /// <param name="generatedTimetable">Generated timetable</param>
        /// <returns>Fitness value</returns>
        private double Fitness(GeneratedTimetable generatedTimetable)
        {
            List<GeneratedCourse> timetableCourses = generatedTimetable.GeneratedCourses;
            double fitness = 0;

            for (int i = 0; i < timetableCourses.Count; i++)
            {
                for (int k = i + 1; k < timetableCourses.Count; k++)
                {
                    if (IsTimeOverlap(timetableCourses[i], timetableCourses[k]))
                    {
                        double overlapTimeInMinutes = CalculateOverLapTime(timetableCourses[i], timetableCourses[k]) * wrongTimeTableMultiplier;
                        if ((timetableCourses[i].LecturerId == timetableCourses[k].LecturerId))
                        {
                            fitness += overlapTimeInMinutes;
                        }
                        if ((timetableCourses[i].RoomId == timetableCourses[k].RoomId))
                        {
                            fitness += overlapTimeInMinutes;
                        }
                        if ((timetableCourses[i].SubjectId == timetableCourses[k].SubjectId) && (timetableCourses[i].CourseType != timetableCourses[k].CourseType))
                        {
                            fitness += overlapTimeInMinutes;
                        }

                        if (timetableCourses[i].SubjectId != timetableCourses[k].SubjectId)
                        {
                            foreach (var group in groups)
                            {
                                var subjectsInGroup = group.GroupLine.Select(x => x.SubjectId);
                                if (subjectsInGroup.Contains(timetableCourses[i].SubjectId) && subjectsInGroup.Contains(timetableCourses[k].SubjectId))
                                    fitness += overlapTimeInMinutes;
                            }
                        }
                    }
                }
                fitness += CheckLecturerAvailability(timetableCourses[i]);
            }


            double roomFitness = GetFitnessFromFreeRoomHeadCount(generatedTimetable.GeneratedCourses);
            double lecturerFitness = GetFitnessFromLecturers(generatedTimetable.GeneratedCourses);
            double groupfitness = GetFitnessFromGroups(generatedTimetable.GeneratedCourses);
            fitness += roomFitness + lecturerFitness + groupfitness;
            
            return fitness;
        }

        /// <summary>
        /// Calculate a fitness value depends on the lecturer availability
        /// </summary>
        /// <param name="generatedCourse"></param>
        /// <returns></returns>
        private double CheckLecturerAvailability(GeneratedCourse generatedCourse)
        {
            Lecturer actLecturer = lecturers.Single(x => x.LecturerId == generatedCourse.LecturerId);
            if (actLecturer.LecturerAvailable == null || actLecturer.LecturerAvailable.Count == 0)
                return 0;

            var availabilities = actLecturer.LecturerAvailable.Where(x => x.DayAvailable == generatedCourse.Day);
            if (availabilities == null || availabilities.Count() == 0)
                return workTimeOneWeekMinute;
            double overTimeMinute = 0;
            double courseStartTime = generatedCourse.StartTime.TotalMinutes;
            double courseEndTime = courseStartTime + generatedCourse.NumberOfClasses * oneClassMinute;
            foreach (var available in availabilities)
            {
                if (available.StartTimeAvailable.TotalMinutes > courseStartTime)
                    overTimeMinute += available.StartTimeAvailable.TotalMinutes - courseStartTime;
                if (available.EndTimeAvailable.TotalMinutes < courseEndTime)
                    overTimeMinute += courseEndTime - available.EndTimeAvailable.TotalMinutes;
            }
            return overTimeMinute * wrongTimeTableMultiplier;
        }

        /// <summary>
        /// Calculate the the overlapped time in minutes
        /// </summary>
        /// <param name="generatedCourse1">Generated course</param>
        /// <param name="generatedCourse2">Generated course</param>
        /// <returns>Time</returns>
        public double CalculateOverLapTime(GeneratedCourse generatedCourse1, GeneratedCourse generatedCourse2)
        {
            double course1StartTimeInMinutes = generatedCourse1.StartTime.TotalMinutes - (pauseTime.TotalMinutes / 2);
            double course1EndTimeInMinutes = course1StartTimeInMinutes + (generatedCourse1.NumberOfClasses * oneClassMinute) + (pauseTime.TotalMinutes / 2);
            double course2StartTimeInMinutes = generatedCourse2.StartTime.TotalMinutes - (pauseTime.TotalMinutes / 2);
            double course2EndTimeInMinutes = course2StartTimeInMinutes + (generatedCourse2.NumberOfClasses * oneClassMinute) + (pauseTime.TotalMinutes / 2);

            if (course1EndTimeInMinutes > course2StartTimeInMinutes && course2StartTimeInMinutes > course1StartTimeInMinutes)
                return course1EndTimeInMinutes - course2StartTimeInMinutes;

            if (course1StartTimeInMinutes > course2StartTimeInMinutes && course1StartTimeInMinutes < course2EndTimeInMinutes)
                return course2EndTimeInMinutes - course1StartTimeInMinutes;

            if (course1StartTimeInMinutes > course2StartTimeInMinutes && course1StartTimeInMinutes < course2EndTimeInMinutes
                && course1EndTimeInMinutes > course2StartTimeInMinutes && course1EndTimeInMinutes < course2EndTimeInMinutes)
                return course1EndTimeInMinutes - course2StartTimeInMinutes + course2EndTimeInMinutes - course1StartTimeInMinutes;

            if (course2StartTimeInMinutes >= course1StartTimeInMinutes && course2StartTimeInMinutes < course1EndTimeInMinutes
                && course2EndTimeInMinutes > course1StartTimeInMinutes && course2EndTimeInMinutes <= course1EndTimeInMinutes)
                return course2EndTimeInMinutes - course1StartTimeInMinutes + course1EndTimeInMinutes - course2StartTimeInMinutes;

            return 0;
        }

        /// <summary>
        /// Calculate the fitness from groups
        /// </summary>
        /// <param name="generatedCourses">List of generated course</param>
        /// <returns>Fitness value</returns>
        private double GetFitnessFromGroups(List<GeneratedCourse> generatedCourses)
        {
            double totalGroupFitness = 0;
            if (groups != null)
            {
                foreach (var group in groups)
                {
                    if (group.GroupLine != null)
                    {
                        foreach (var subject in group.GroupLine.Select(gl => gl.Subject))
                        {
                            if (subject.Course != null)
                            {
                                foreach (WeekDay item in Enum.GetValues(typeof(WeekDay)))
                                {
                                    List<GeneratedCourse> generatedCoursesInOneGroup = (from genCourse in generatedCourses
                                                                                        join course in subject.Course
                                                                                        on genCourse.CourseId equals course.CourseId
                                                                                        select genCourse).ToList();
                                    generatedCoursesInOneGroup.Sort((x, y) => TimeSpan.Compare(x.StartTime, y.StartTime));
                                    if (generatedCoursesInOneGroup.Count > 1)
                                    {
                                        for (int i = 0; i < generatedCoursesInOneGroup.Count - 1; i++)
                                        {
                                            totalGroupFitness += (int)((generatedCoursesInOneGroup[i + 1].StartTime + TimeSpan.FromMinutes(45 * generatedCoursesInOneGroup[i + 1].NumberOfClasses)) - generatedCoursesInOneGroup[i].StartTime).TotalMinutes;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return totalGroupFitness / oneHourInMinute;
        }

        /// <summary>
        /// Calculate the fitness from lectureres
        /// </summary>
        /// <param name="generatedCourses">List of generated course</param>
        /// <returns>Fitness value</returns>
        private double GetFitnessFromLecturers(List<GeneratedCourse> generatedCourses)
        {
            double totalLecturerAvailable = 0;
            double totalNegativeAvailableMinutes = 0;
            foreach (var actLecturer in lecturers)
            {
                double lecturereAvailableInMinutes = workTimeOneWeekMinute;
                if (actLecturer.LecturerAvailable != null && actLecturer.LecturerAvailable.Count > 0)
                    lecturereAvailableInMinutes = actLecturer.LecturerAvailable.Sum(x => (x.EndTimeAvailable - x.StartTimeAvailable).TotalMinutes);
                List<GeneratedCourse> actGenCourses = generatedCourses.Where(x => x.LecturerId == actLecturer.LecturerId).ToList();
                foreach (var actCourse in actGenCourses)
                {
                    lecturereAvailableInMinutes -= courses.Single(x => x.SubjectId == actCourse.SubjectId && x.CourseId == actCourse.CourseId).NumberOfClasses * oneClassMinute;
                }
                if (lecturereAvailableInMinutes < 0)
                    totalNegativeAvailableMinutes += lecturereAvailableInMinutes;
                totalLecturerAvailable += lecturereAvailableInMinutes;
            }
            if (totalNegativeAvailableMinutes < 0)
                return totalLecturerAvailable = lecturers.Count * workTimeOneWeekMinute + Math.Abs(totalNegativeAvailableMinutes);
            return totalLecturerAvailable / oneHourInMinute;
        }

        /// <summary>
        /// Calculate the fitness from rooms
        /// </summary>
        /// <param name="generatedCourses">List of generated course</param>
        /// <returns>Fitness value</returns>
        private double GetFitnessFromFreeRoomHeadCount(List<GeneratedCourse> generatedCourses)
        {
            int freeSpace = 0;
            int[] roomCount = new int[rooms.Count];
            foreach (var item in generatedCourses)
            {
                Room actRoom = rooms.Single(x => x.RoomId == item.RoomId);
                freeSpace = actRoom.HeadCount - courses.Single(x => x.CourseId == item.CourseId).HeadCount;
                roomCount[rooms.IndexOf(actRoom)] = freeSpace;
            }
            return roomCount.Max() - roomCount.Min();
        }

    }
}
