﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public struct GeneratedCourse
    {
        int subjectId;
        int courseId;
        int roomId;
        int lecturerId;
        WeekDay day;
        TimeSpan startTime;
        int numberOfClasses;
        int headCount;
        bool timeFix;
        CourseType courseType;

        public int CourseId { get => courseId; set => courseId = value; }
        public int RoomId { get => roomId; set => roomId = value; }
        public int LecturerId { get => lecturerId; set => lecturerId = value; }
        public WeekDay Day { get => day; set => day = value; }
        public TimeSpan StartTime { get => startTime; set => startTime = value; }
        public bool TimeFix { get => timeFix; set => timeFix = value; }
        public int SubjectId { get => subjectId; set => subjectId = value; }
        public int NumberOfClasses { get => numberOfClasses; set => numberOfClasses = value; }
        public int HeadCount { get => headCount; set => headCount = value; }
        public CourseType CourseType { get => courseType; set => courseType = value; }
    }
}
