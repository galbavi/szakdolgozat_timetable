﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;
using OfficeOpenXml;


namespace BusinessLogic.ExcelExport
{
    class ExcelExportManagement
    {
        string filePath;
        ExcelPackage excel;
        List<TimetableLine> courses;
        FileInfo excelFile;

        public ExcelExportManagement(string filePath)
        {
            this.filePath = filePath;
            excel = new ExcelPackage();
        }

        public void ExportTimetableToExcel(TimetableHeader timetable)
        {
            courses = timetable.TimetableLine.ToList();
            courses.Sort((x, y) => x.Day == y.Day ? TimeSpan.Compare(x.StartTime, y.StartTime) : (x.Day < y.Day ? -1 : 1));

            excel.Workbook.Worksheets.Add(timetable.Name);
            var xlWorkSheet = excel.Workbook.Worksheets[timetable.Name];

            CreateHeader(xlWorkSheet);
            CreateBody(xlWorkSheet);
            excelFile = new FileInfo(filePath + string.Format("\\{0}_{1}{2}{3}_{4}{5}{6}.xlsx",timetable.Name.Replace(" ", string.Empty), timetable.ProductionDate.Year,
                GetTwoPlacesInt(timetable.ProductionDate.Month), GetTwoPlacesInt(timetable.ProductionDate.Day), GetTwoPlacesInt(timetable.ProductionDate.Hour), 
                GetTwoPlacesInt(timetable.ProductionDate.Minute), GetTwoPlacesInt(timetable.ProductionDate.Second)));
            excel.SaveAs(excelFile);

            OpenFileInExcel();
        }

        private string GetTwoPlacesInt(int number)
        {
            return number < 10 ? "0" + number : number.ToString();
        }

        private void OpenFileInExcel()
        {
            bool isExcelInstalled = Type.GetTypeFromProgID("Excel.Application") != null ? true : false;
            if (isExcelInstalled)
            {
                System.Diagnostics.Process.Start(excelFile.ToString());
            }
        }

        private void CreateBody(ExcelWorksheet xlWorkSheet)
        {
            int rowNumber = 2;
            foreach (var course in courses)
            {
                List<string[]> bodyRow = new List<string[]>()
                {
                  new string[] { course.SubjectName, course.CourseName, EnumExtension.GetDescription(course.Day), EnumExtension.GetDescription(course.Type),
                      course.StartTime.ToString() ,(course.StartTime + TimeSpan.FromMinutes(45 * course.NumberOfClasses)).ToString(),
                      course.NumberOfClasses.ToString(), course.HeadCount.ToString(), course.LecturerName.ToString(), course.RoomName }
                };

                string bodyRange = "A" + rowNumber + ":" + Char.ConvertFromUtf32(bodyRow[0].Length + 64) + rowNumber;
                xlWorkSheet.Cells[bodyRange].LoadFromArrays(bodyRow);
                rowNumber++;
            }
            xlWorkSheet.Cells.AutoFitColumns();

        }

        private void CreateHeader(ExcelWorksheet xlWorkSheet)
        {
            List<string[]> headerRow = new List<string[]>()
            {
              new string[] { "Tantárgy neve","Kurzus neve", "Nap", "Kurzus Típusa", "Kezdés időpontja", "Befejezés időpontja", "Órák száma", "Maximális Létszám", "Tanár Neve", "Tanterem Neve" }
            };

            string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
            xlWorkSheet.Cells[headerRange].LoadFromArrays(headerRow);
            xlWorkSheet.Cells[headerRange].Style.Font.Bold = true;
            xlWorkSheet.Cells[headerRange].Style.Font.Size = 12;
            xlWorkSheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Black);
        }
    }
}
