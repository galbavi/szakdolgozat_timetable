﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Data.Interfaces;
using Data.Models;
using Data.Repositories;

namespace BusinessLogic.DataMangement
{
    public class SubjectManagement
    {
        ISubjectRepository subjectRepository;
        ILecturerRepository lecturerRepository;

        public SubjectManagement(TimetableGeneratorContext entities)
        {
            subjectRepository = new SubjectRepository(entities);
            lecturerRepository = new LecturerRepository(entities);
        }

        public ObservableCollection<Subject> GetAllSubject()
        {
            return new ObservableCollection<Subject>(subjectRepository.All);
        }

        public void RemoveSubject(Subject selectedSubject)
        {
            subjectRepository.Delete(selectedSubject);
        }

        public void AddNewSubject(Subject newSubject)
        {
            subjectRepository.Insert(newSubject);
        }

        public void ModifySubject(int subjectId, Subject modifiedSubject)
        {
            subjectRepository.Modify(subjectId, modifiedSubject);
        }

        public ObservableCollection<Lecturer> GetAllLecturer()
        {
            return new ObservableCollection<Lecturer>(lecturerRepository.All);
        }
    }
}
