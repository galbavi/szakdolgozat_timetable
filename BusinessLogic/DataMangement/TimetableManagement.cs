﻿using BusinessLogic.CalendarModel;
using BusinessLogic.ExcelExport;
using Data;
using Data.Interfaces;
using Data.Models;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.DataMangement
{
    public class TimetableManagement
    {
        Generator timetableGenerator;
        ITimetableHeaderRepository timetableHeaderRepository;

        public TimetableManagement(TimetableGeneratorContext entities)
        {
            timetableGenerator = new Generator();
            timetableHeaderRepository = new TimetableHeaderRepository(entities);
        }
        
        public TimetableHeader GenerateNewTimetable(string newTimetableName)
        {
            TimetableHeader newTimetable = timetableGenerator.Generate(newTimetableName);
            timetableHeaderRepository.Insert(newTimetable);
            return newTimetable;
        }

        public ObservableCollection<TimetableHeader> GetAllTimetable()
        {
            return new ObservableCollection<TimetableHeader>(timetableHeaderRepository.All);
        }

        public void DeleteTimetable(TimetableHeader selectedTimeTable)
        {
            timetableHeaderRepository.Delete(selectedTimeTable);
        }

        public CalendarCourseList GetCoursesFromTimetableToCalendar(TimetableHeader selectedTimeTable)
        {
            if (selectedTimeTable.TimetableLine == null || selectedTimeTable.TimetableLine.Count == 0)
                return null;
            CalendarCourseList newCalendarCourses = new CalendarCourseList();
            foreach (var course in selectedTimeTable.TimetableLine)
            {
                CalendarCourse newCalendarCourse = new CalendarCourse();
                newCalendarCourse.WeekDay = course.Day;
                newCalendarCourse.StartTime = course.StartTime;
                newCalendarCourse.EndTime = course.StartTime + TimeSpan.FromMinutes(45 * course.NumberOfClasses);
                newCalendarCourse.CourseInformation = course.ToString();
                newCalendarCourses.Add(newCalendarCourse);
            }
            return newCalendarCourses;
        }

        public void ExportToExcel(string selectedPath, TimetableHeader selectedTimeTable)
        {
            ExcelExportManagement excelExportManagement = new ExcelExportManagement(selectedPath);
            excelExportManagement.ExportTimetableToExcel(selectedTimeTable);
        }
    }
}
