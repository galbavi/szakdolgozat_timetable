﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Data.Interfaces;
using Data.Models;
using Data.Repositories;

namespace BusinessLogic.DataMangement
{
    public class GroupManagement
    {
        IGroupHeaderRepository groupHeaderRepository;
        ISubjectRepository subjectRepository;

        public GroupManagement(TimetableGeneratorContext entities)
        {
            groupHeaderRepository = new GroupHeaderRepository(entities);
            subjectRepository = new SubjectRepository(entities);
        }

        public ObservableCollection<Subject> GetAllSubject()
        {
            return new ObservableCollection<Subject>(subjectRepository.All);
        }

        public void DeleteGroup(GroupHeader groupHeaderToDelete)
        {
            groupHeaderRepository.Delete(groupHeaderToDelete);
        }

        public ObservableCollection<GroupHeader> GetAllGroup()
        {
            return new ObservableCollection<GroupHeader>(groupHeaderRepository.All);
        }

        public void DeleteAllGroup(ObservableCollection<GroupHeader> groupHeaders)
        {
            groupHeaderRepository.DeleteMore(groupHeaders);
        }

        public void AddNewGroup(GroupHeader newGroupHeader)
        {
            groupHeaderRepository.Insert(newGroupHeader);
        }

        public void ModifyGroup(int groupHeaderId, GroupHeader modifyGroupHeader)
        {
            groupHeaderRepository.Modify(groupHeaderId, modifyGroupHeader);
        }
    }
}
