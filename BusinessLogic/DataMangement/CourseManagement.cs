﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Data.Interfaces;
using Data.Models;
using Data.Repositories;

namespace BusinessLogic.DataMangement
{
    public class CourseManagement
    {
        ICourseRepository courseRepository;
        IRoomRepository roomRepository;

        public CourseManagement(TimetableGeneratorContext entities)
        {
            courseRepository = new CourseRepository(entities);
            roomRepository = new RoomRepository(entities);
        }

        public ObservableCollection<Room> GetAllRooms()
        {
            return new ObservableCollection<Room>(roomRepository.All);
        }

        public void AddNewCourse(Course newCourse)
        {
            courseRepository.Insert(newCourse);
        }

        public void ModifyCourse(int courseId, Course modifiedCourse)
        {
            courseRepository.Modify(courseId, modifiedCourse);
        }

        public void RemoveCourse(Course selectedCourse)
        {
            courseRepository.Delete(selectedCourse);
        }

        internal List<Course> GetAllCourse()
        {
            return new List<Course>(courseRepository.All);
        }

        public bool CheckCourseHeadCount(Course editorCourse)
        {
            return roomRepository.All.Count(x => x.HeadCount >= editorCourse.HeadCount && x.RoomType.Select(t => t.Type).Contains(editorCourse.Type)) > 0;
        }

        public bool CheckCourseNumberOfClasses(Course editorCourse)
        {
            TimeSpan endTime;
            if (editorCourse.StartTime != null && editorCourse.StartTime > TimeSpan.Zero)
                endTime = editorCourse.StartTime.Value + TimeSpan.FromMinutes(editorCourse.NumberOfClasses * 45);
            else
                endTime = TimeSpan.FromHours(8) + TimeSpan.FromMinutes(editorCourse.NumberOfClasses * 45);

            return (endTime <= TimeSpan.FromHours(18)); 
        }

        public bool CheckRoomCourseConnectHeadCount(Course editorCourse, ObservableCollection<RoomCourseConnect> editorRoomCourseConnects)
        {
            if (editorRoomCourseConnects == null || editorRoomCourseConnects.Count == 0)
                return true;

            return editorRoomCourseConnects.Select(x => x.Room).Count(y => y.HeadCount < editorCourse.HeadCount) == 0;
        }

        public bool CheckRoomType(Course editorCourse, Room selectedRoom)
        {
            return selectedRoom.RoomType.Select(x => x.Type).Contains(editorCourse.Type);
        }

        public bool CheckRoomCourseConnectTypes(Course editorCourse, ObservableCollection<RoomCourseConnect> editorRoomCourseConnects)
        {
            if (editorRoomCourseConnects == null || editorRoomCourseConnects.Count == 0)
                return true;

            foreach (var room in editorRoomCourseConnects.Select(x => x.Room))
            {
                if (!room.RoomType.Select(x => x.Type).Contains(editorCourse.Type))
                    return false;
            }
            return true;
        }
    }
}
