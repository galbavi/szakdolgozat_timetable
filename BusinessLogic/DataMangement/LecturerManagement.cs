﻿using Data;
using Data.Interfaces;
using Data.Models;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.DataMangement
{
    public class LecturerManagement
    {
        private ILecturerRepository lecturerRepository;

        public LecturerManagement(TimetableGeneratorContext entities)
        {
            lecturerRepository = new LecturerRepository(entities);
        }

        public void DeleteLecturer(Lecturer selectedLecturer)
        {
            lecturerRepository.Delete(selectedLecturer);
        }

        public ObservableCollection<Lecturer> GetAllLecturer()
        {
            return new ObservableCollection<Lecturer>(lecturerRepository.All);
        }

        public void AddNewLecturer(Lecturer newLecturer)
        {
            lecturerRepository.Insert(newLecturer);
        }

        public void ModifyLecturer(int lecturerId, Lecturer modifiedLecturer)
        {
            lecturerRepository.Modify(lecturerId, modifiedLecturer);
        }

        public bool CheckLecutrerTypes(ObservableCollection<LecturerType> editorLecturerTypes)
        {
            return editorLecturerTypes != null && editorLecturerTypes.Count() > 0;
        }
    }
}
