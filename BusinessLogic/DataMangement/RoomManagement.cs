﻿using Data.Interfaces;
using Data.Repositories;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;
using System.Collections.ObjectModel;

namespace BusinessLogic.DataMangement
{
    public class RoomManagement
    {
        private IRoomRepository roomRepository;
        private ICourseRepository courseRepository;

        public RoomManagement(TimetableGeneratorContext entities)
        {
            roomRepository = new RoomRepository(entities);
            courseRepository = new CourseRepository(entities);
        }

        public void DeleteRoom(Room selectedRoom)
        {
            roomRepository.Delete(selectedRoom);
        }

        public ObservableCollection<Room> GetAllRoom()
        {
            return new ObservableCollection<Room>(roomRepository.All);
        }

        public void AddNewRoom(Room newRoom)
        {
            roomRepository.Insert(newRoom);
        }

        public void ModifyRoom(int roomId, Room modifiedRoom)
        {
            roomRepository.Modify(roomId, modifiedRoom);
        }

        public bool CheckRoomTypes(ObservableCollection<RoomType> editorRoomTypes)
        {
            return editorRoomTypes != null && editorRoomTypes.Count() > 0;
        }

        public bool CheckCourseHeadCounts(Room editorRoom)
        {
            return editorRoom.RoomCourseConnect.Select(x => x.Course).Count(y => y.HeadCount > editorRoom.HeadCount) == 0;
        }
    }
}
